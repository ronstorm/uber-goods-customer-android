package com.uber.goods.customer;

import com.uber.goods.customer.util.CommonUtils;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class CommonUtilsTest {

    private CommonUtils cu;

    @Before
    public void setUp(){
        cu = new CommonUtils();
    }

    @Test
    public void testIsEmailValid() throws Exception {
        String invalidEmail []  = new String[] { "wellTravel", "wellTravel@.com.my", "wellTravel123@.com", "wellTravel123@.com.com",
                ".wellTravel@wellTravel.com", "wellTravel()*@gmail.com", "wellTravel@%*.com",
                "wellTravel..2002@gmail.com", "wellTravel.@gmail.com",
                "wellTravel@wellTravel@gmail.com", "-wellTravel@gmail.com" };

        String validEmail [] = new String[] { "wellTravel@yahoo.com",
                "wellTravel-100@yahoo.com", "wellTravel.100@yahoo.com",
                "wellTravel111@wellTravel.com", "wellTravel-100@wellTravel.net",
                "wellTravel.100@wellTravel.com.au", "wellTravel@1.com",
                "wellTravel@gmail.com.com", "wellTravel+100@gmail.com",
                "wellTravel-100@yahoo-test.com" };

        for(String email: invalidEmail){
            boolean testFlag = cu.isEmailValid(email);
            assertTrue(!testFlag);
        }
        for(String email: validEmail){
            boolean testFlag = cu.isEmailValid(email);
            assertTrue(testFlag);
        }
    }//end of testIsEmailValid method

    @Test
    public void testIsPhoneNumberValid() throws Exception{
        String validPhoneNumber [] = new String[]{
            "1234567890","123-456-7890", "123-456-7890", "123-456-7890",
                "(123)-456-7890", "123 456 7890"
        };

        for(String pn : validPhoneNumber){
            boolean testFlag = cu.isPhoneNumberValid(pn);
            assertTrue(testFlag);
        }

        String inValidPhoneNumber [] = new String[]{
                "12345678901","1231-456-7890", "123-456-78901", "1234-456-7890",
                "(1231)-456-7890", "1233 456 7890"
        };

        for(String pn : inValidPhoneNumber){
            boolean testFlag = cu.isPhoneNumberValid(pn);
            assertTrue(!testFlag);
        }
    }//end of testIsPhoneNumberValid method

    @Test
    public void testIsPasswordValid() throws  Exception{
        String validPassword [] = new String[] {
           "Bangladesh@2018", "#4jhhj&H123", "7777GGG@@@@0000KKKs"
        };

        for(String password : validPassword){
            boolean testFlag = cu.isPasswordValid(password);
            assertTrue(testFlag);
        }

        String inValidPassword [] = new String[] {
                "Bangladesh2018", "4jhhjH123", "GGG@@@@KKKs"
        };

        for(String password : inValidPassword){
            boolean testFlag = cu.isPasswordValid(password);
            assertTrue(!testFlag);
        }
    }

    @Test
    public void testfindNumbersFromString() throws Exception{
        String str = "abcc12mnd13jddk15";
        List<Integer> numList = cu.findNumbersFromString(str);
        assertTrue("Size should be three (3)",numList.size() == 3);
        assertTrue("First list element is 12",numList.get(0) == 12);
        assertTrue("Second list element is 13",numList.get(1) == 13);
        assertTrue("Third list element is 15",numList.get(2) == 15);
    }
}