package com.uber.goods.customer;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by anonymous on 12/18/17.
 *
 */

interface NetworkAdapterTestService {
    @GET("/")
    Call<ResponseBody> healthCheck();
}
