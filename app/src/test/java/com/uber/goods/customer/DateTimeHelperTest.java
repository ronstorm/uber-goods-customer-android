package com.uber.goods.customer;

import com.uber.goods.customer.util.DateTimeHelper;

import org.junit.Test;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class DateTimeHelperTest {
    private final DateTimeHelper dateTimeHelper = new DateTimeHelper();

    @Test
    public void testStringToDate(){
        Date currentDate = Calendar.getInstance().getTime();
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

        String patternStr = "dd/MM/yyyy";

        Date date = dateTimeHelper.stringToDate(format.format(currentDate), patternStr);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String expectedDate = formatter.format(date);
        String dateToTest = formatter.format(currentDate);

        assertEquals("Parsing string to date failed", expectedDate, dateToTest);
    }

    @Test
    public void testDateToString(){
        Date currentDate = Calendar.getInstance().getTime();
        String patternStr = "dd/MM/yyyy";
        String expectedDateStr = dateTimeHelper.dateToString(currentDate, patternStr);

        Date testDate = Calendar.getInstance().getTime();
        String testPatternStr = "dd/MM/yyyy";
        DateFormat format = new SimpleDateFormat(testPatternStr, Locale.ENGLISH);
        String testDateStr = format.format(testDate);

        assertEquals("Converts date to string failed", expectedDateStr, testDateStr);

        String expectedDateStrWithoutPattern = dateTimeHelper.dateToString(currentDate, "");
        assertEquals("Converts date to string failed without pattern string", expectedDateStrWithoutPattern, testDateStr);
    }

    @Test
    public void testGetYearFromDate(){
        Date currentDate = Calendar.getInstance().getTime();
        int expectedYear = dateTimeHelper.getYearFromDate(currentDate);

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        int testedYear = calendar.get(Calendar.YEAR);

        assertThat("expected year not equal to tested year",testedYear, is(expectedYear));
        assertThat("current year should not be 1999",expectedYear, is(not(1999)));
    }

    @Test
    public void testGetMonthFromDate(){
        Date currentDate = Calendar.getInstance().getTime();
        int expectedMonth = dateTimeHelper.getMonthFromDate(currentDate);

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        int testedMonth = calendar.get(Calendar.MONTH) +1 ;

        assertThat("expected month not equal to tested month",testedMonth, is(expectedMonth));
        assertThat("expected month should not be 13",expectedMonth, is(not(13)));
    }

    @Test
    public void testGetDayFromDate(){
        Date currentDate = Calendar.getInstance().getTime();
        int expectedDay = dateTimeHelper.getDayFromDate(currentDate);

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        int testedDay = calendar.get(Calendar.DAY_OF_MONTH) ;

        assertThat("expected day not equal to tested day",expectedDay, is(testedDay));
        assertThat("expected day should not be 32",expectedDay, is(not(32)));
    }

    @Test
    public void testGetMonthNameFromDate(){
        Date currentDate = Calendar.getInstance().getTime();
        String expectedMonthName = dateTimeHelper.getMonthNameFromDate(currentDate);

        Format formatter = new SimpleDateFormat("MMMM", Locale.ENGLISH);
        String testedMonthName = formatter.format(currentDate);

        assertThat("expected month name not equal to tested month name",expectedMonthName, is(testedMonthName));
    }
}
