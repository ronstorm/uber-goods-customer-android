package com.uber.goods.customer;

import org.junit.Test;

import java.io.IOException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;

import static org.junit.Assert.assertTrue;

/**
 * Created by anonymous on 12/10/17.
 *
 */

public class NetworkAdpaterTest {
    @Test
    public void test() throws IOException {
        MockWebServer mockWebServer = new MockWebServer();

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
        OkHttpClient client = builder.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("https://welltravel.staging.welltravel.com/health/").toString())
                .client(client)
                .build();

        //Set a response for retrofit to handle. You can copy a sample
        //response from your server to simulate a correct result or an error.
        //MockResponse can also be customized with different parameters
        //to match your test needs
        mockWebServer.enqueue(new MockResponse().setBody(""));

        NetworkAdapterTestService service = retrofit.create(NetworkAdapterTestService.class);

        //With your service created you can now call its method that should
        //consume the MockResponse above. You can then use the desired
        //assertion to check if the result is as expected. For example:
        System.out.println("testing");

        assertTrue(service.healthCheck().execute().body() != null);

        //Finish web server
        mockWebServer.shutdown();
    }
}
