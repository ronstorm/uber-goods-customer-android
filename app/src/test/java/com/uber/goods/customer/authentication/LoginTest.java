package com.uber.goods.customer.authentication;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.model.authentication.CustomerLoginResponse;
import com.uber.goods.customer.model.authentication.LoginCredentials;
import com.uber.goods.customer.model.authentication.LoginResponse;
import com.uber.goods.customer.network.service.LoginService;

import org.junit.Test;

import java.io.IOException;

import io.reactivex.functions.Consumer;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static org.junit.Assert.assertTrue;

/**
 * Created by anonymous on 12/18/17.
 *
 */

public class LoginTest {
    @Test
    public void Logintest() throws IOException {
        MockWebServer mockWebServer = new MockWebServer();
        TestObserver<LoginResponse> mockObserver = new TestObserver<>();
        TestScheduler moskScheduler = new TestScheduler();

        mockWebServer.enqueue(new MockResponse().setBody(""));

//        LoginCredentials loginCredentials = new LoginCredentials();
//        AppEngine.getInstance().networkAdapter.create(LoginService.class).login(loginCredentials)
//                .subscribeOn(moskScheduler)
//                .observeOn(Schedulers.io())
//                .subscribe((Consumer<? super CustomerLoginResponse>) mockObserver);

        assertTrue(mockObserver.hasSubscription());
        assertTrue(!mockObserver.isTimeout());

        mockWebServer.shutdown();
    }
}
