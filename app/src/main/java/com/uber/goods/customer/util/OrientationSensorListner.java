package com.uber.goods.customer.util;

/**
 * Created by bitto on 17-Apr-18.
 */

public interface OrientationSensorListner {
    public void onRotationChange(float rotation);
}
