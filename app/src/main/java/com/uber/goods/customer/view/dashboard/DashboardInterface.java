package com.uber.goods.customer.view.dashboard;

import com.uber.goods.customer.model.dashboard.GoodsCategory;
import com.uber.goods.customer.model.dashboard.PaymentRequest;
import com.uber.goods.customer.model.user.DriverProfile;

import java.util.HashMap;
import java.util.List;

/**
 * Created by bitto on 17-Apr-18.
 */

public interface DashboardInterface {
    public void setDirection(List<List<HashMap<String, String>>> routes);
    public void tripPaymentSuccess();
    public void tripPaymentError(PaymentRequest paymentRequest);
    public void fetchDriverInfo(DriverProfile driverProfile);
}
