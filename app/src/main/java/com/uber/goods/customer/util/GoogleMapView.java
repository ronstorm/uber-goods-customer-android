package com.uber.goods.customer.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.BuildConfig;
import com.uber.goods.customer.R;

/**
 * Created by anonymous on 12/19/17.
 * This is the Google Map View Utility
 */

public class GoogleMapView implements LocationListener {

    private static final int LOCATION_UPDATE_MIN_DISTANCE = 0;
    private static final int LOCATION_UPDATE_MIN_TIME = 10000;

    private LocationManager mLocationManager;
    private Context context;
    private boolean permission;
    private Activity activity;
    private boolean mapCameraOffset;
    private GoogleMapLocationListner googleMapLocationListner;

    private static final String MAP_VIEW_BUNDLE_KEY = BuildConfig.GoogleApiKey;
    public AlertDialog.Builder builder;
    public static boolean dialog;
    LocationChangeListner locationChangeListner;

    public GoogleMapView() {
        mapCameraOffset = false;
    }

    private void initView(Context context, boolean permission) {
        this.context = context;;
        this.permission = permission;
        if(permission) {
            mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("location: ","changed");
        if (location != null) {
            if(googleMapLocationListner!=null) {
                googleMapLocationListner.onLocationChanged(location);
            }
            if(locationChangeListner!=null) {
                locationChangeListner.onLocationChangedApp(location);
            }
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    private void getCurrentLocation(Activity activity) {
        Log.d("location: ","attached");
        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        Location location = null;
        if (!(isGPSEnabled || isNetworkEnabled)) {
            if(!dialog) this.displayPromptForEnablingGPS(activity);
        } else {
            if (isNetworkEnabled) {
                if (ActivityCompat.checkSelfPermission(activity.getApplication().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                this.mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, this);
                location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

            if (isGPSEnabled) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, this);
                location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
        if (location != null) {
            if(googleMapLocationListner!=null) {
                googleMapLocationListner.onLocationChanged(location);
            }
            if(locationChangeListner!=null) {
                locationChangeListner.onLocationChangedApp(location);
            }
        }
    }

    public void requestPermissionCallback(int requestCode, int[] grantResults, Activity activity) {
        this.activity=activity;
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AppEngine.getInstance().googleMapView.initView(activity, true);
                } else {
                    AppEngine.getInstance().googleMapView.initView(activity, false);
                }
            }
        }
    }

    public void displayPromptForEnablingGPS(final Activity activity)
    {

        builder =  new AlertDialog.Builder(activity);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = "Do you want open GPS setting?";

        builder.setMessage(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                GoogleMapView.dialog = false;
                                activity.startActivity(new Intent(action));
                                d.dismiss();
                            }
                        }).setCancelable(false);
        builder.create().show();
        GoogleMapView.dialog = true;
    }

    public void detachMap() {
        if(this.mLocationManager!=null) this.mLocationManager.removeUpdates(this);
    }

    public void refreshLocManager(Activity activity) {
        if(permission && mLocationManager!=null) {
            boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (isGPSEnabled && isNetworkEnabled) {
                mLocationManager = (LocationManager) activity.getApplication().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            }
            getCurrentLocation(activity);
        }
    }

    public void setMapCameraOffset(boolean mapCameraOffset) {
        this.mapCameraOffset = mapCameraOffset;
    }

    public void setGoogleMapLocationListner(GoogleMapLocationListner googleMapLocationListner) {
        this.googleMapLocationListner = googleMapLocationListner;
    }

    public LocationChangeListner getLocationChangeListner() {
        return locationChangeListner;
    }

    public void setLocationChangeListner(LocationChangeListner locationChangeListner) {
        this.locationChangeListner = locationChangeListner;
    }
}
