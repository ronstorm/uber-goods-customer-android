package com.uber.goods.customer.activity.dashboard;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.activity.BaseUIController;
import com.uber.goods.customer.activity.mytrips.Rating;
import com.uber.goods.customer.model.dashboard.PaymentRequest;
import com.uber.goods.customer.model.user.DriverProfile;
import com.uber.goods.customer.presenter.dashboard.DashboardPresenter;
import com.uber.goods.customer.util.Animation;
import com.uber.goods.customer.util.GmapView;
import com.uber.goods.customer.util.GoogleMapLocationListner;
import com.uber.goods.customer.util.OrientationSensor;
import com.uber.goods.customer.util.OrientationSensorListner;
import com.uber.goods.customer.util.TImerUtilInterface;
import com.uber.goods.customer.view.dashboard.DashboardInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import paytabs.project.PayTabActivity;
//import paytabs.project.PayTabActivity;

/**
 * Created by kud-wtag on 3/4/18.
 */

public class DashBoardActivity extends BaseUIController implements GoogleMapLocationListner, OrientationSensorListner, DashboardInterface, GoogleMap.OnMapLoadedCallback {

    @InjectView(R.id.requestDeliveryBtn)
    Button requestDeliveryBtn;

    @InjectView(R.id.findRiderLoader)
    LinearLayout findRiderLoader;

    @InjectView(R.id.driverSelectedSection)
    LinearLayout driverSelectedSection;

    @InjectView(R.id.findRiderImg3)
    ImageView findRiderImg3;

    @InjectView(R.id.findRiderImg2)
    ImageView findRiderImg2;

    @InjectView(R.id.findRiderImg1)
    ImageView findRiderImg1;

    @InjectView(R.id.clientName)
    TextView clientName;

    @InjectView(R.id.deliveryCancel)
    public Button deliveryCancel;

    @InjectView(R.id.driverOnTheWay)
    public LinearLayout driverOnTheWay;

    @InjectView(R.id.driverOnTheWayDelivery)
    public LinearLayout driverOnTheWayDelivery;

    @InjectView(R.id.activityRating)
    public ScrollView activityRating;

    @InjectView(R.id.cashBtn)
    public Button cashBtn;

    @InjectView(R.id.cardBtn)
    public Button cardBtn;

    @InjectView(R.id.driverImage)
    public ImageView driveImage;

    @InjectView(R.id.driverName)
    public TextView driverName;

    @InjectView(R.id.driverImageOtw)
    public ImageView driverImageOtw;

    @InjectView(R.id.driverNameOtw)
    public TextView driverNameOtw;

    @InjectView(R.id.driverImageOtwd)
    public ImageView driverImageOtwd;

    @InjectView(R.id.driverNameOtwd)
    public TextView driverNameOtwd;

    @InjectView(R.id.driverImageRating)
    public ImageView driverImageRating;

    @InjectView(R.id.driverNameRating)
    public TextView driverNameRating;

    @InjectView(R.id.callDriver)
    public Button callDriver;

    @InjectView(R.id.cancelRide)
    public Button cancelRide;

    @InjectView(R.id.callDriverOtw)
    public Button callDriverOtw;

    @InjectView(R.id.cancelRideOtw)
    public Button cancelRideOtw;

    @InjectView(R.id.callDriverOtwd)
    public Button callDriverOtwd;

    @InjectView(R.id.totalFare)
    public TextView totalFare;

    @InjectView(R.id.ratingBar)
    public RatingBar ratingBar;

    @InjectView(R.id.ratingComment)
    public EditText ratingComment;

    int total_image = 3;
    int current_image = 0;
    int alpha = 0;
    boolean driverFound = false;

    GmapView gmapView;
    OrientationSensor orientationSensor;

    double destinationLat = 23.862595;
    double destinationLng = 90.401154;
    boolean dragCamera = true;
    PolylineOptions lineOptions;
    Polyline polyline;
    Location location;

    Marker marker;
    Marker pickUpMarker;
    Marker destinationMarker;
    Marker driverMarker;

    float bearing;
    float rotation;

    DashboardPresenter dashboardPresenter;
    BroadcastReceiver receiver;

    double tripPickup[];
    double tripDest[];
    double[] driverLocation;
    boolean driverLocationInit = true;
    int tripState = 0;
    DriverProfile driverProfile;

    @Override
    public void onStart() {
        super.onStart();
        final Context finalContext = this;
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                        onReceived(context, intent);
                    Toast.makeText(finalContext, "driver found", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter("NotificationReceived")
        );
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.DRAWER_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_dashboard);
        this.init(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean dragCamera = true;
        AppEngine.getInstance().googleMapView.setGoogleMapLocationListner(this);
        orientationSensor.init(this, this, this);
    }

    @Override
    protected void onDestroy() {
        this.orientationSensor.sensorDetach();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        this.orientationSensor.sensorDetach();
        super.onPause();
    }

    private void init(Bundle savedInstanceState) {
        tripDest = new double[2];
        tripPickup = new double[2];
        driverLocation = new double[2];
        ButterKnife.inject(this);
        gmapView = new GmapView();
        gmapView.setLoadCustomMarker(true);
        gmapView.initView(this, (MapView) findViewById(R.id.mapView), savedInstanceState, true);
        gmapView.getMapView().onStart();
        AppEngine.getInstance().googleMapView.setGoogleMapLocationListner(this);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        clientName.setText(AppEngine.getInstance().constants.clientName);
        Picasso.with(this).load(AppEngine.getInstance().constants.userImageLink).into(this.getUserImage());
        orientationSensor = new OrientationSensor();
        dashboardPresenter = new DashboardPresenter(this, this);

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null && bundle.getString("id")!=null) {
            if(bundle.getString("id").equals("1")) {
                AppEngine.getInstance().sharedPrefUtils.putPref("tripId", bundle.getString("tripId"), this);
                AppEngine.getInstance().sharedPrefUtils.putPref("driverId", bundle.getString("driverId"), this);
                AppEngine.getInstance().sharedPrefUtils.putPref("pickUp", bundle.getString("pickUp"), getApplicationContext());
                AppEngine.getInstance().sharedPrefUtils.putPref("destination", bundle.getString("destination"), getApplicationContext());
                AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "deliveryAccepted", getApplicationContext());
            } else if(bundle.getString("id").equals("2")) {
                AppEngine.getInstance().sharedPrefUtils.putPref("tripId", bundle.getString("tripId"), this);
                AppEngine.getInstance().sharedPrefUtils.putPref("driverId", bundle.getString("driverId"), this);
                AppEngine.getInstance().sharedPrefUtils.putPref("pickUp", bundle.getString("pickUp"), getApplicationContext());
                AppEngine.getInstance().sharedPrefUtils.putPref("destination", bundle.getString("destination"), getApplicationContext());
                AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "onTheWay", getApplicationContext());
            } else if(bundle.getString("id").equals("3")) {
                AppEngine.getInstance().sharedPrefUtils.putPref("tripId", bundle.getString("tripId"), this);
                AppEngine.getInstance().sharedPrefUtils.putPref("driverId", bundle.getString("driverId"), this);
                AppEngine.getInstance().sharedPrefUtils.putPref("pickUp", bundle.getString("pickUp"), getApplicationContext());
                AppEngine.getInstance().sharedPrefUtils.putPref("destination", bundle.getString("destination"), getApplicationContext());
                AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "onTheWayDelivery", getApplicationContext());
            } else if(bundle.getString("id").equals("4")) {
                AppEngine.getInstance().sharedPrefUtils.putPref("tripId", bundle.getString("tripId"), this);
                AppEngine.getInstance().sharedPrefUtils.putPref("driverId", bundle.getString("driverId"), this);
                AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "tripEnded", getApplicationContext());
                AppEngine.getInstance().sharedPrefUtils.putPref("total_fare", bundle.getString("total_fare"), getApplicationContext());
            } else if(bundle.getString("id").equals("5")) {
                AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "", getApplicationContext());
            }
            AppEngine.getInstance().sharedPrefUtils.putPref("tripId", bundle.getString("tripId"), getApplicationContext());
            AppEngine.getInstance().sharedPrefUtils.putPref("pickUp", bundle.getString("pickUp"), getApplicationContext());
            AppEngine.getInstance().sharedPrefUtils.putPref("destination", bundle.getString("destination"), getApplicationContext());
        }
        if(AppEngine.getInstance().sharedPrefUtils.getPref("tripState", this).equals("deliveryRequested")) {
            AppEngine.getInstance().animation.expand(findRiderLoader, this);
            findRiderImg1.setImageAlpha(alpha);
            findRiderImg2.setImageAlpha(alpha);
            findRiderImg3.setImageAlpha(alpha);
            startAnimation();
            requestDeliveryBtn.setVisibility(View.INVISIBLE);
        }
        if(AppEngine.getInstance().sharedPrefUtils.getPref("tripState", this).equals("deliveryAccepted")) {
            tripState=1;
            this.driverFound = true;
            requestDeliveryBtn.setVisibility(View.INVISIBLE);
            AppEngine.getInstance().animation.expand(driverSelectedSection, this);
            dashboardPresenter.getDriverProfile(AppEngine.getInstance().sharedPrefUtils.getPref("driverId", this));
        }
        if(AppEngine.getInstance().sharedPrefUtils.getPref("tripState", this).equals("onTheWay")) {
            tripState=2;
            this.driverFound = true;
            requestDeliveryBtn.setVisibility(View.INVISIBLE);
            AppEngine.getInstance().animation.expand(driverOnTheWay, this);
            dashboardPresenter.getDriverProfile(AppEngine.getInstance().sharedPrefUtils.getPref("driverId", this));
        }
        if(AppEngine.getInstance().sharedPrefUtils.getPref("tripState", this).equals("onTheWayDelivery")) {
            tripState=3;
            this.driverFound = true;
            requestDeliveryBtn.setVisibility(View.INVISIBLE);
            AppEngine.getInstance().animation.expand(driverOnTheWayDelivery, this);
            dashboardPresenter.getDriverProfile(AppEngine.getInstance().sharedPrefUtils.getPref("driverId", this));
        }
        if(AppEngine.getInstance().sharedPrefUtils.getPref("tripState", this).equals("tripEnded")) {
            tripState=0;
            this.driverFound = true;
            requestDeliveryBtn.setVisibility(View.INVISIBLE);
            AppEngine.getInstance().animation.expand(activityRating, this);
            dashboardPresenter.getDriverProfile(AppEngine.getInstance().sharedPrefUtils.getPref("driverId", this));
            totalFare.setText("SAR "+AppEngine.getInstance().sharedPrefUtils.getPref("total_fare", this));
        }
        if(!AppEngine.getInstance().sharedPrefUtils.getPref("pickUp", this).equals("")) {
            //gmapView.getGoogleMap().clear();
            String[] pick = AppEngine.getInstance().sharedPrefUtils.getPref("pickUp", this).replace("[", "").replace("]", "").split(",");
            String[] dest = AppEngine.getInstance().sharedPrefUtils.getPref("destination", this).replace("[", "").replace("]", "").split(",");
            Log.d("pickdest", pick[1]+","+pick[0]+" - "+dest[1]+","+dest[0]);
            dashboardPresenter.getTripDirection(pick[1]+","+pick[0], dest[1]+","+dest[0]);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1676: {
                executeCall();
                return;
            }
            default:
                AppEngine.getInstance().googleMapView.requestPermissionCallback(requestCode, grantResults, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (gmapView.getGoogleMap() != null && AppEngine.getInstance().constants.LOCATION!=null) {
            LatLng gps = new LatLng(AppEngine.getInstance().constants.LOCATION.getLatitude(), AppEngine.getInstance().constants.LOCATION.getLongitude());

            if(dragCamera) {
                marker = gmapView.getGoogleMap().addMarker(new MarkerOptions()
                        .position(gps)
                        .title("My Location")
                        .icon(BitmapDescriptorFactory.defaultMarker(184)));

                gmapView.getGoogleMap().animateCamera(CameraUpdateFactory.newLatLngZoom(gps, 12));
                dragCamera=false;
            } else {
                GmapView.animateMarker(AppEngine.getInstance().constants.LOCATION, marker);
            }

            location = AppEngine.getInstance().constants.LOCATION;
        }
    }

    private void setMarkers() {

        LatLng gps = null;
        if(tripState>0 && lineOptions!=null) {
            if(polyline!=null) polyline.remove();
            //gps = new LatLng(AppEngine.getInstance().constants.LOCATION.getLatitude(), AppEngine.getInstance().constants.LOCATION.getLongitude());

            polyline = gmapView.getGoogleMap().addPolyline(lineOptions);

            if(tripState>0 && tripState<3) {
                if(pickUpMarker!=null) pickUpMarker.remove();
                gps = new LatLng(tripPickup[0], tripPickup[1]);
                if(tripState>0) pickUpMarker = gmapView.getGoogleMap().addMarker(new MarkerOptions()
                        .position(gps)
                        .title("Pickup")
                        .icon(BitmapDescriptorFactory.defaultMarker(184)));
            }
            if(destinationMarker!=null) destinationMarker.remove();
            gps = new LatLng(tripDest[0], tripDest[1]);
            if(tripState>0) destinationMarker = gmapView.getGoogleMap().addMarker(new MarkerOptions()
                    .position(gps)
                    .title("Destination")
                    .icon(BitmapDescriptorFactory.defaultMarker(0)));

        }

        gps = new LatLng(driverLocation[0], driverLocation[1]);
        if(driverLocationInit || driverMarker==null) {
            if(tripState>0) driverMarker = gmapView.getGoogleMap().addMarker(new MarkerOptions()
                    .position(gps)
                    .title("Driver Location")
                    .flat(true)
                    .anchor(0.5f, 0.5f)
                    .rotation(rotation)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.car)));
            driverLocationInit = false;
        } else {
            Location temp = new Location(LocationManager.GPS_PROVIDER);
            temp.setLatitude(driverLocation[0]);
            temp.setLongitude(driverLocation[1]);
            temp.setBearing(bearing);
            driverMarker.setRotation(rotation);
            GmapView.animateMarker(temp, driverMarker);
        }
    }

    @OnClick(R.id.requestDeliveryBtn)
    public void requestDeliveryBtnOnclick() {
        Intent intent = new Intent(this, RequestDeliveryActivity.class);
        startActivityForResult(intent, 1);
    }

    @OnClick(R.id.deliveryCancel)
    public void deliveryCancelOnClick() {
        AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "", this);
        AppEngine.getInstance().animation.collapse(findRiderLoader, this);
        driverFound = true;
        requestDeliveryBtn.setVisibility(View.VISIBLE);
        dashboardPresenter.cancelTrip();
    }

    @OnClick(R.id.cancelRideOtw)
    public void cancelRideOtwOnClick() {
        dashboardPresenter.cancelTrip();
        endTrip();
    }

    @OnClick(R.id.cancelRide)
    public void cancelRideOnClick() {
        dashboardPresenter.cancelTrip();
        endTrip();
    }

    @OnClick(R.id.cardBtn)
    public void cardBtnOnClick() {
        if(AppEngine.getInstance().sharedPrefUtils.getPref("orderId", this).equals("")) {
            AppEngine.getInstance().sharedPrefUtils.putPref("orderId", UUID.randomUUID().toString(), this);

        }
        Intent in = new Intent(this, PayTabActivity.class);
        in.putExtra("pt_merchant_email", "bitto.kazi@gmail.com"); //this a demo account for
        in.putExtra("pt_secret_key", "iD4vZsexRogBimeG64jjQdvH5oBIqtEI6Ie6bsD3RTEafTQmY9FOnbvsA7a0AOIHxWNJprNgkrrAlhWjrC8OOEgpLrpmg84O4Rxu");//Add your Secret Key Here
        in.putExtra("pt_transaction_title", driverProfile.getUser().getName());
        in.putExtra("pt_amount", AppEngine.getInstance().sharedPrefUtils.getPref("total_fare", this));
        in.putExtra("pt_currency_code", "USD"); //Use Standard 3 character ISO
        in.putExtra("pt_shared_prefs_name", "ug_customer_shared");
        in.putExtra("pt_customer_email", driverProfile.getUser().getEmail());
        in.putExtra("pt_customer_phone_number", "0097300001");
        in.putExtra("pt_order_id", AppEngine.getInstance().sharedPrefUtils.getPref("orderId", this));
        in.putExtra("pt_product_name", "UG Ride");
        in.putExtra("pt_timeout_in_seconds", "300"); //Optional
//Billing Address
        in.putExtra("pt_address_billing", "Flat 1,Building 123, Road 2345");
        in.putExtra("pt_city_billing", "Juffair");
        in.putExtra("pt_state_billing", "Manama");
        in.putExtra("pt_country_billing", "BHR");
        in.putExtra("pt_postal_code_billing", "00973"); //Put Country Phone code if Postal code not available '00973'
//Shipping Address
        in.putExtra("pt_address_shipping", "Flat 1,Building 123, Road 2345");
        in.putExtra("pt_city_shipping", "Juffair");
        in.putExtra("pt_state_shipping", "Manama");
        in.putExtra("pt_country_shipping", "BHR");
        in.putExtra("pt_postal_code_shipping", "00973"); //Put Country Phone code if Postal code not available '00973'
        int requestCode = 69;
        startActivityForResult(in, requestCode);
    }

    @OnClick(R.id.cashBtn)
    public void cashBtnOnClick() {
        showloading();
        if(AppEngine.getInstance().sharedPrefUtils.getPref("orderId", this).equals("")) {
            AppEngine.getInstance().sharedPrefUtils.putPref("orderId", UUID.randomUUID().toString(), this);
        }
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setMedium("0");
        paymentRequest.setTransactionId("n/a");
        paymentRequest.setOrderId(AppEngine.getInstance().sharedPrefUtils.getPref("orderId", this));
        showloading();
        dashboardPresenter.tripPayment(paymentRequest);
    }

    @OnClick(R.id.callDriver)
    public void callDriverOnclick() {
        executeCall();
    }

    @OnClick(R.id.callDriverOtw)
    public void callDriverOtwOnclick() {
        executeCall();
    }

    @OnClick(R.id.callDriverOtwd)
    public void callDriverOtwdOnclick() {
        executeCall();
    }

    private void executeCall() {
        if(driverProfile != null) {
            final Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + driverProfile.getMobileNumber()));
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        1676);
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(intent);
                    }
                });
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                AppEngine.getInstance().animation.expand(findRiderLoader, this);
                findRiderImg1.setImageAlpha(alpha);
                findRiderImg2.setImageAlpha(alpha);
                findRiderImg3.setImageAlpha(alpha);
                startAnimation();
//                AppEngine.getInstance().timerUtil.initTimeCustomTask(this, 5000, new TImerUtilInterface() {
//                    @Override
//                    public void executeTask() {
//                        driverFound = true;
//                        AppEngine.getInstance().animation.slideDown(findRiderLoader, 500);
//                        AppEngine.getInstance().animation.slideUp(driverSelectedSection, 500);
//                    }
//                });
                AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "deliveryRequested", this);
                requestDeliveryBtn.setVisibility(View.INVISIBLE);
                driverFound = false;
            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                //if there's no result
            }
        } else if (requestCode == 69) {
            //if(resultCode == Activity.RESULT_OK){
                SharedPreferences shared_prefs = getSharedPreferences("ug_customer_shared", MODE_PRIVATE);
                String pt_response_code = shared_prefs.getString("pt_response_code", "");
                String pt_transaction_id = shared_prefs.getString("pt_transaction_id", "");
                Toast.makeText(this, "PayTabs Response Code : " + pt_response_code, Toast.LENGTH_LONG).show();
                Toast.makeText(this, "Paytabs transaction ID after payment : " + pt_transaction_id, Toast.LENGTH_LONG).show();

                if(pt_response_code.equals("100")) {
                    AppEngine.getInstance().sharedPrefUtils.putPref("tripTransactionId", pt_transaction_id, this);
                    AppEngine.getInstance().sharedPrefUtils.putPref("paymentMedium", "1", this);
                    PaymentRequest paymentRequest = new PaymentRequest();
                    paymentRequest.setMedium("1");
                    paymentRequest.setTransactionId(AppEngine.getInstance().sharedPrefUtils.getPref("tripTransactionId", this));
                    paymentRequest.setOrderId(AppEngine.getInstance().sharedPrefUtils.getPref("orderId", this));
                    showloading();
                    dashboardPresenter.tripPayment(paymentRequest);
                }

            //}
            //else if (resultCode == Activity.RESULT_CANCELED) {

            //}
        }
    }
    public void startAnimation() {
        //AppEngine.getInstance().timerUtil.stoptimertask();
        AppEngine.getInstance().timerUtil.initTimeCustomTask(this, 10, new TImerUtilInterface() {
            @Override
            public void executeTask() {
                alpha += 5;
                if (current_image == 1) {
                    findRiderImg1.setImageAlpha(alpha);
                } else if (current_image == 2) {
                    findRiderImg2.setImageAlpha(alpha);
                } else if (current_image == 3) {
                    findRiderImg3.setImageAlpha(alpha);
                } else if (current_image == 4) {
                    findRiderImg1.setImageAlpha(0);
                    findRiderImg2.setImageAlpha(0);
                    findRiderImg3.setImageAlpha(0);
                    current_image = 0;
                }
                if (alpha >= 250) {
                    alpha = 0;
                    current_image++;
                }
                if(!driverFound) startAnimation();
            }
        });
    }

    @Override
    public void onRotationChange(float rotation) {
        String currentPosition = this.getResources().getString(R.string.share_location_cur_loc_text);
        if(AppEngine.getInstance().constants.LOCATION!=null) {

        }
    }

    @Override
    public void setDirection(List<List<HashMap<String, String>>> routes) {
        ArrayList<LatLng> points = null;
        lineOptions = null;
        MarkerOptions markerOptions = new MarkerOptions();

        if(routes!=null) {
            for(int i=0;i<routes.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = routes.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){

                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);


                    points.add(position);

                    if(i==0 && j==0) {
                        tripPickup[0] = Double.parseDouble(point.get("lat"));
                        tripPickup[1] = Double.parseDouble(point.get("lng"));
                    }
                    if(i==routes.size()-1 && j==path.size()-1) {
                        tripDest[0] = Double.parseDouble(point.get("lat"));
                        tripDest[1] = Double.parseDouble(point.get("lng"));
                    }
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLACK);
            }

            Log.d("pickup",AppEngine.getInstance().sharedPrefUtils.getPref("pickUp", this));
            Log.d("dest", AppEngine.getInstance().sharedPrefUtils.getPref("destination", this));

            // Drawing polyline in the Google Map for the i-th route
            setMarkers();
        }
    }

    @Override
    public void tripPaymentSuccess() {
        Rating rating = new Rating();
        rating.setTrip(Integer.parseInt(AppEngine.getInstance().sharedPrefUtils.getPref("tripId", this)));
        rating.setComment(this.ratingComment.getText().toString());
        rating.setRating((int)ratingBar.getRating());
        this.dashboardPresenter.submitRating(rating);
        Toast.makeText(this, "Payment Successful", Toast.LENGTH_LONG).show();
        hideloading();
        endTrip();
    }

    @Override
    public void tripPaymentError(final PaymentRequest paymentRequest) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create(); //Read Update
        alertDialog.setTitle("Payment");
        alertDialog.setMessage("Payment not Completed yet. Click below to continue");

        alertDialog.setButton("Confirm", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dashboardPresenter.tripPayment(paymentRequest);
            }
        });

        alertDialog.show();
    }

    @Override
    public void fetchDriverInfo(DriverProfile driverProfile) {
        this.driverProfile = driverProfile;
        Picasso.with(this).load(driverProfile.getUser().getPicture()).into(driveImage);
        driverName.setText(driverProfile.getUser().getName());

        Picasso.with(this).load(driverProfile.getUser().getPicture()).into(driverImageOtw);
        driverNameOtw.setText(driverProfile.getUser().getName());

        Picasso.with(this).load(driverProfile.getUser().getPicture()).into(driverImageOtwd);
        driverNameOtwd.setText(driverProfile.getUser().getName());

        Picasso.with(this).load(driverProfile.getUser().getPicture()).into(driverImageRating);
        driverNameRating.setText(driverProfile.getUser().getName());
    }

    @Override
    public void onBackPressed(){
        finishAffinity();
    }

    public void onReceived(Context context, Intent intent) {
        if(!intent.getExtras().getString("id").equals("8")) {
            if(intent.getExtras().getInt("tripState")==1) {
                tripState=1;
                driverFound = true;
                AppEngine.getInstance().animation.collapse(findRiderLoader, this);
                AppEngine.getInstance().animation.expand(driverSelectedSection, this);
                dashboardPresenter.getDriverProfile(AppEngine.getInstance().sharedPrefUtils.getPref("driverId", this));
            } else if(intent.getExtras().getInt("tripState")==2) {
                tripState=2;
                driverFound = true;
                AppEngine.getInstance().animation.collapse(driverSelectedSection, this);
                AppEngine.getInstance().animation.expand(driverOnTheWay, this);
                dashboardPresenter.getDriverProfile(AppEngine.getInstance().sharedPrefUtils.getPref("driverId", this));
            } else if(intent.getExtras().getInt("tripState")==3) {
                tripState=3;
                if(pickUpMarker!=null) pickUpMarker.remove();
                driverFound = true;
                AppEngine.getInstance().animation.collapse(driverOnTheWay, this);
                AppEngine.getInstance().animation.expand(driverOnTheWayDelivery, this);
                dashboardPresenter.getDriverProfile(AppEngine.getInstance().sharedPrefUtils.getPref("driverId", this));
            } else if(intent.getExtras().getInt("tripState")==4) {
                tripState=0;
                driverFound = true;
                Animation animation = new Animation();
                animation.collapse(driverOnTheWayDelivery, this);
                AppEngine.getInstance().animation.expand(activityRating, this);
                dashboardPresenter.getDriverProfile(AppEngine.getInstance().sharedPrefUtils.getPref("driverId", this));
                totalFare.setText("SAR "+AppEngine.getInstance().sharedPrefUtils.getPref("total_fare", this));
            } else if(intent.getExtras().getInt("tripState")==5) {
                tripState=0;
                endTrip();
            }
            if(!AppEngine.getInstance().sharedPrefUtils.getPref("pickUp", this).equals("")) {
                //gmapView.getGoogleMap().clear();
                String[] pick = AppEngine.getInstance().sharedPrefUtils.getPref("pickUp", this).replace("[", "").replace("]", "").split(",");
                String[] dest = AppEngine.getInstance().sharedPrefUtils.getPref("destination", this).replace("[", "").replace("]", "").split(",");
                if(tripState>1) {
                    dest = AppEngine.getInstance().sharedPrefUtils.getPref("destination", this).replace("[", "").replace("]", "").split(",");
                }
                Log.d("pickdest", pick[1]+","+pick[0]+" - "+dest[1]+","+dest[0]);
                dashboardPresenter.getTripDirection(pick[1]+","+pick[0], dest[1]+","+dest[0]);
            }
        }
        if(intent.getExtras().getString("id").equals("8")) {
            if(!AppEngine.getInstance().sharedPrefUtils.getPref("pickUp", this).equals("") && tripState>2) {
                //gmapView.getGoogleMap().clear();
                String[] dest = AppEngine.getInstance().sharedPrefUtils.getPref("destination", this).replace("[", "").replace("]", "").split(",");
                Gson gson = new Gson();
                Map<String, Double> loc = gson.fromJson(intent.getExtras().getString("location"), Map.class);

                Log.d("pickdest", String.valueOf(loc.get("latitude"))+","+String.valueOf(loc.get("longitude"))+" - "+dest[1]+","+dest[0]);
                dashboardPresenter.getTripDirection(String.valueOf(loc.get("latitude"))+","+String.valueOf(loc.get("longitude")), dest[1]+","+dest[0]);
            }
            Gson gson = new Gson();
            Map<String, Double> loc = gson.fromJson(intent.getExtras().getString("location"), Map.class);
            bearing = Float.parseFloat(intent.getExtras().getString("bearing"));
            rotation = Float.parseFloat(intent.getExtras().getString("rotation"));
            driverLocation[0] = loc.get("latitude");
            driverLocation[1] = loc.get("longitude");
        }
        setMarkers();
    }

    public void endTrip() {
        driverFound = true;
        lineOptions = null;
        AppEngine.getInstance().sharedPrefUtils.putPref("pickUp", "", this);
        AppEngine.getInstance().sharedPrefUtils.putPref("destination", "", this);

        AppEngine.getInstance().animation.collapse(findRiderLoader, this);
        AppEngine.getInstance().animation.collapse(driverSelectedSection, this);
        AppEngine.getInstance().animation.collapse(driverOnTheWay, this);
        AppEngine.getInstance().animation.collapse(driverOnTheWayDelivery, this);
        AppEngine.getInstance().animation.collapse(activityRating, this);
        requestDeliveryBtn.setVisibility(View.VISIBLE);
        AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "", this);
        AppEngine.getInstance().sharedPrefUtils.putPref("tripTransactionId", "", this);
        AppEngine.getInstance().sharedPrefUtils.putPref("orderId", "", this);
        AppEngine.getInstance().sharedPrefUtils.putPref("driverId", "", this);
        if(driverMarker!=null) driverMarker.remove();
        if(pickUpMarker!=null) pickUpMarker.remove();
        if(destinationMarker!=null) destinationMarker.remove();
        if(polyline!=null) polyline.remove();
    }

    @Override
    public void onMapLoaded() {
        setMarkers();
    }
}
