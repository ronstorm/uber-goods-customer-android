package com.uber.goods.customer.model.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripEstimate {
    @SerializedName("pickup_address")
    @Expose
    private String pickupAddress;
    @SerializedName("destination_address")
    @Expose
    private String destinationAddress;
    @SerializedName("good_category")
    @Expose
    private Integer goodCategory;
    @SerializedName("good_sub_category")
    @Expose
    private Integer goodSubCategory;
    @SerializedName("product_size")
    @Expose
    private Integer productSize;
    @SerializedName("product_weight")
    @Expose
    private Integer productWeight;
    @SerializedName("worker_quantity")
    @Expose
    private Integer workerQuantity;
    @SerializedName("delivery_type")
    @Expose
    private String deliveryType;

    public String getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public Integer getGoodCategory() {
        return goodCategory;
    }

    public void setGoodCategory(Integer goodCategory) {
        this.goodCategory = goodCategory;
    }

    public Integer getGoodSubCategory() {
        return goodSubCategory;
    }

    public void setGoodSubCategory(Integer goodSubCategory) {
        this.goodSubCategory = goodSubCategory;
    }

    public Integer getProductSize() {
        return productSize;
    }

    public void setProductSize(Integer productSize) {
        this.productSize = productSize;
    }

    public Integer getProductWeight() {
        return productWeight;
    }

    public void setProductWeight(Integer productWeight) {
        this.productWeight = productWeight;
    }

    public Integer getWorkerQuantity() {
        return workerQuantity;
    }

    public void setWorkerQuantity(Integer workerQuantity) {
        this.workerQuantity = workerQuantity;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }
}
