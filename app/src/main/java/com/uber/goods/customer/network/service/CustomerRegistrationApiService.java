package com.uber.goods.customer.network.service;


import com.uber.goods.customer.model.GeoLocationResponse;
import com.uber.goods.customer.model.user.Customer;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by kud-wtag on 3/7/18.
 */

public interface CustomerRegistrationApiService {

    @GET("/maps/api/geocode/json")
    Observable<GeoLocationResponse> getAddressFromLatLng(@Query("latlng") String latlng, @Query("key") String key);

    @POST("/api/v1/customer/")
    Observable<Response<Customer>> register(@Body Customer customer);

//    @Multipart
//    @POST("/api/v1/customer/")
//    Observable<Response<Customer>> register(
//            @Part("user.name") String userName,
//            @Part("user.email") String email,
//            @Part("user.password") String password,
//            @Part MultipartBody.Part picture,
//            @Part("gender") String gender,
//            @Part("birth_date") String birthDate,
//            @Part("nationality") String nationality,
//            @Part("address") String adderss,
//            @Part("mobile_number") String mobileNumber);

}
