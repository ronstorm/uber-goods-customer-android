package com.uber.goods.customer.activity.authentication;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.model.authentication.ForgetPassword;
import com.uber.goods.customer.model.authentication.ForgetPasswordResponse;
import com.uber.goods.customer.presenter.authentication.ForgetPasswordPresenter;
import com.uber.goods.customer.view.authentication.ForgetPasswordCallback;

/**
 * Created by bitto on 14-Mar-18.
 */

public class ForgetPasswordRecoveryFragment extends Fragment implements View.OnClickListener, ForgetPasswordCallback {

    View view;
    Context context;
    Button buttonNext;
    EditText forgetEmail;
    ForgetPasswordMainActivity forgetPasswordMainActivity;
    ForgetPasswordPresenter forgetPasswordPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_forget_password, container, false);
        this.init();
        return view;
    }

    public void init(){
        buttonNext = view.findViewById(R.id.nextBtn);
        forgetEmail = view.findViewById(R.id.forgetEmail);
        buttonNext.setOnClickListener(this);
        forgetPasswordPresenter = new ForgetPasswordPresenter(context, this);
    }

    public ForgetPasswordRecoveryFragment setContext(Context context, ForgetPasswordMainActivity forgetPasswordMainActivity) {
        this.context = context;
        this.forgetPasswordMainActivity = forgetPasswordMainActivity;
        return this;
    }

    @Override
    public void onClick(View view) {
        if(view.equals(buttonNext)) {
            forgetPasswordMainActivity.showloading();
            ForgetPassword forgetPassword = new ForgetPassword();
            forgetPassword.setEmail(forgetEmail.getText().toString());
            AppEngine.getInstance().constants.forgetEmail = forgetEmail.getText().toString();
            forgetPasswordPresenter.forgetPassword(forgetPassword);
        }
    }

    @Override
    public void success() {
        forgetPasswordMainActivity.hideloading();
        this.forgetPasswordMainActivity.selectFrag(1);
    }

    @Override
    public void error() {
        Toast.makeText(context, "Not Found", Toast.LENGTH_SHORT).show();
        forgetPasswordMainActivity.hideloading();
    }
}
