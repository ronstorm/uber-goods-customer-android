package com.uber.goods.customer.util;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by anonymous on 12/7/17.
 *
 */

public class TimerUtil {

    private Timer timer;
    private TimerTask timerTask;
    private final Handler handler = new Handler();

    public TimerUtil() {

    }
    public <T> void startTimer(Context context, Class<T> activity, long time) {
        timer = new Timer();
        initializeTimerTask(context, activity);
        timer.schedule(timerTask, time); //
    }

    public void stoptimertask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private <T> void initializeTimerTask(final Context context, final Class<T> activity) {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        Intent intent = new Intent(context, activity);
                        context.startActivity(intent);
                        stoptimertask();
                    }
                });
            }
        };
    }

    public <T> void initTimeCustomTask(final Context context, int time, final TImerUtilInterface tImerUtilInterface) {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        tImerUtilInterface.executeTask();
                    }
                });
            }
        };
        timer.schedule(timerTask, time);
    }
}
