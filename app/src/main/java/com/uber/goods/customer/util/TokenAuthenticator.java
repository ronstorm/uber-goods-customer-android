package com.uber.goods.customer.util;

import android.content.Context;
import android.net.Proxy;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.BuildConfig;
import com.uber.goods.customer.model.authentication.CustomerLoginResponse;
import com.uber.goods.customer.network.service.LoginService;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by bitto on 07-May-18.
 */

public class TokenAuthenticator implements Interceptor {
    private Context context;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response mainResponse = chain.proceed(chain.request());
        Request mainRequest = chain.request();
        if (mainResponse.code() == 401 || mainResponse.code() == 403) {
            System.out.println("::::::::::::::REFRESHING TOKEN:::::::::::::");
            //refresh access token via refreshtoken

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(BuildConfig.UberGoodsBaseURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            LoginService service = client.create(LoginService.class);
            Call<CustomerLoginResponse> refreshTokenResult=service.refreshToken(AppEngine.getInstance().sharedPrefUtils.getPref(AppEngine.getInstance().constants.OAUTH_REFRESH_TOKEN, context), AppEngine.getInstance().constants.OAUTH_CLIENT_ID, AppEngine.getInstance().constants.OAUTH_CLIENT_SECRET, AppEngine.getInstance().constants.OAUTH_REFRESHT_GRANT_TYPE);
            //this is syncronous retrofit request
            CustomerLoginResponse refreshResult = refreshTokenResult.execute().body();
            //check if response equals 400 , mean empty response
            if(refreshResult!=null) {
                AppEngine.getInstance().sharedPrefUtils.putPref(AppEngine.getInstance().constants.OAUTH_ACCESS_TOKEN, refreshResult.getAccessToken(), context);
                AppEngine.getInstance().sharedPrefUtils.putPref(AppEngine.getInstance().constants.OAUTH_TOKEN_TYPE, refreshResult.getTokenType(), context);
                AppEngine.getInstance().sharedPrefUtils.putPref(AppEngine.getInstance().constants.OAUTH_REFRESH_TOKEN, refreshResult.getRefreshToken(), context);

                //save new access and refresh token
                // than create a new request and modify it accordingly using the new token
                Request.Builder builder = mainRequest.newBuilder().header("Authorization", AppEngine.getInstance().sharedPrefUtils.getPref(AppEngine.getInstance().constants.OAUTH_TOKEN_TYPE, context)+" "+AppEngine.getInstance().sharedPrefUtils.getPref(AppEngine.getInstance().constants.OAUTH_ACCESS_TOKEN, context)).
                        method(mainRequest.method(), mainRequest.body());
                mainResponse = chain.proceed(builder.build());
            }
        }
        return mainResponse;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
