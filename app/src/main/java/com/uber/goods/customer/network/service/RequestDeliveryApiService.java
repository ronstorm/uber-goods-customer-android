package com.uber.goods.customer.network.service;

import com.uber.goods.customer.model.dashboard.GoodsCategory;
import com.uber.goods.customer.model.dashboard.PaymentRequest;
import com.uber.goods.customer.model.dashboard.PaymentResponse;
import com.uber.goods.customer.model.dashboard.RequestedTripResponse;
import com.uber.goods.customer.model.dashboard.TripEstimate;
import com.uber.goods.customer.model.dashboard.TripEstimateResponse;
import com.uber.goods.customer.model.dashboard.TripRequest;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by kud-wtag on 6/12/18.
 */

public interface RequestDeliveryApiService {
    @GET("/api/v1/good-categories/")
    Observable<List<GoodsCategory>> getAllGoodsCategory();

    @POST("/api/v1/recurring-trip/")
    Observable<TripRequest> requestRecurringTrip(@Body TripRequest tripRequest);

    @POST("/api/v1/trip/")
    Observable<TripRequest> requestTrip(@Body TripRequest tripRequest);

    @POST("/api/v1/trip/estimation/")
    Observable<TripEstimateResponse> tripEstimation(@Body TripEstimate tripEstimate);

    @POST("/api/v1/trip/{id}/payment/")
    Observable<PaymentResponse> tripPayment(@Body PaymentRequest paymentRequest, @Path("id") String tripId);
}
