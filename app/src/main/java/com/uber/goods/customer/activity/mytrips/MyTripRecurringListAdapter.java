package com.uber.goods.customer.activity.mytrips;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.model.mytrips.TripItem;

import java.util.List;

import io.reactivex.annotations.NonNull;

/**
 * Created by kud-wtag on 6/24/18.
 */

class ViewHolderRecur {
    TextView txtName;
    ImageView userImage;
    TextView txtDate;
    TextView pickUp;
    TextView destination;
    TextView tripId;
    TextView goodType;
    TextView amount;
    TextView distanceTime;
}

public class MyTripRecurringListAdapter extends BaseAdapter {
    private static List<TripItem> myTripList;
    private LayoutInflater mInflater;
    private Context context;

    public MyTripRecurringListAdapter(Context context, List<TripItem> values) {
        this.myTripList = values;
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return myTripList.size();
    }

    @Override
    public Object getItem(int position) {
        return myTripList.get(position);
    }

    @NonNull
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolderRecur holder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.my_trip_listview_recurring, parent, false);
            holder = new ViewHolderRecur();
            holder.txtName = convertView.findViewById(R.id.txtName);
            holder.userImage = convertView.findViewById(R.id.userImage);
            holder.txtDate  = convertView.findViewById(R.id.txtDate);
            holder.pickUp = convertView.findViewById(R.id.pickUp);
            holder.destination = convertView.findViewById(R.id.destination);
            holder.tripId = convertView.findViewById(R.id.tripId);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolderRecur) convertView.getTag();
        }

        if(myTripList.get(position).getDriverProfile()!=null) {
            holder.txtName.setText(myTripList.get(position).getDriverProfile().getUser().getName());
            Picasso.with(context).load(myTripList.get(position).getDriverProfile().getUser().getPicture()).into(holder.userImage);
        }
        //holder.txtDate.setText(myTripList.get(position).getName());
        holder.pickUp.setText(myTripList.get(position).getPickupAddress());
        holder.destination.setText(myTripList.get(position).getDestinationAddress());
        holder.tripId.setText(String.valueOf("Trip #"+myTripList.get(position).getId()));

        return convertView;
    }
}
