package com.uber.goods.customer.activity.authentication;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bigkoo.pickerview.MyOptionsPickerView;
import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.activity.BaseUIController;
import com.uber.goods.customer.model.user.Customer;
import com.uber.goods.customer.model.user.User;
import com.uber.goods.customer.presenter.authentication.CustomerRegistrationPresenter;
import com.uber.goods.customer.util.GoogleMapLocationListner;
import com.uber.goods.customer.view.authentication.CustomerRegistrationInterface;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by bitto on 03-Mar-18.
 */

public class CustomerRegistrationActivity extends BaseUIController implements CustomerRegistrationInterface, GoogleMapLocationListner {

    @InjectView(R.id.birthDate)
    EditText birthDate;

    @InjectView(R.id.gender)
    EditText gender;

    @InjectView(R.id.uploadOptionHolder)
    RelativeLayout uploadOptionHolder;

    @InjectView(R.id.uploadOption)
    LinearLayout uploadOption;

    @InjectView(R.id.addProfilePucture)
    ImageView addProfilePucture;

    @InjectView(R.id.showProfilePicture)
    ImageView showProfilePicture;

    @InjectView(R.id.cameraBtn)
    LinearLayout cameraBtn;

    @InjectView(R.id.imageBtn)
    LinearLayout imageBtn;

    @InjectView(R.id.TOC)
    RadioButton toc;

    @InjectView(R.id.goBackBtn)
    Button goBackBtn;

    @InjectView(R.id.signInBtn)
    Button signInBtn;

    @InjectView(R.id.defaultLocation)
    EditText defaultLocation;

    @InjectView(R.id.mobileNumber)
    EditText mobileNumber;

    @InjectView(R.id.nationality)
    EditText nationality;

    @InjectView(R.id.name)
    EditText name;

    @InjectView(R.id.email)
    EditText email;

    @InjectView(R.id.password)
    EditText password;

    @InjectView(R.id.confirmPassword)
    EditText confirmPassword;

    @InjectView(R.id.signUpBtn)
    Button signUpBtn;

    List<String> genders = new ArrayList<String>();
    private CustomerRegistrationPresenter customerRegistrationPresenter;

    View vMasker;
    MyOptionsPickerView singlePicker;

    private boolean tocStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.EMPTY_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_customer_registration);
        this.init();
    }

    private void init() {
        ButterKnife.inject(this);
        tocStatus = false;
        customerRegistrationPresenter = new CustomerRegistrationPresenter(this, this, this);
        singlePicker = customerRegistrationPresenter.initPicker(this, gender);
        AppEngine.getInstance().googleMapView.setGoogleMapLocationListner(this);
        if(!AppEngine.getInstance().constants.CURRENT_LOCATION.equals(""))this.customerRegistrationPresenter.getDefaultLocation(AppEngine.getInstance().constants.CURRENT_LOCATION);
    }

    @OnClick(R.id.gender)
    public void onGenderClick() {
        singlePicker.show();
    }

    @OnClick(R.id.uploadOptionHolder)
    public void uploadOptionHolderClick() {
        this.customerRegistrationPresenter.uploadOptionHolderClick(this, uploadOption, uploadOptionHolder);
    }

    @OnClick(R.id.addProfilePucture)
    public void addProfilePuctureClick() {
        this.customerRegistrationPresenter.addProfilePictureClick(this, uploadOption, uploadOptionHolder);
    }

    @OnClick(R.id.cameraBtn)
    public void cameraBtnClick() {
        this.customerRegistrationPresenter.requestPermission(this);
    }

    @OnClick(R.id.imageBtn)
    public void imageBtnClick() {
        AppEngine.getInstance().androidIntent.startFileSelectActivity(this, AppEngine.getInstance().constants.MESSAGE_ACTIVITY_RESULT_CODE_FILE);
    }

    @OnClick(R.id.TOC)
    public void tocClick() {
        this.toc.setChecked(tocStatus = !tocStatus);
    }

    @OnClick(R.id.goBackBtn)
    public void goBackBtnClick() {
        this.finish();
    }

    @OnClick(R.id.signInBtn)
    public void setSignInBtnClick() {
        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.customerRegistrationPresenter.uploadOptionSelectRusult(requestCode, resultCode, data, showProfilePicture, uploadOptionHolder, uploadOption);
    }

    @Override
    public void setDefaultLocation(String location) {
        this.defaultLocation.setText(location);
    }

    @Override
    public void registrationSuccess(Customer customer) {
        hideloading();
        Toast.makeText(this, "Success and id "+customer.getId(), Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void restrationError() {
        hideloading();
        Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        this.customerRegistrationPresenter.getDefaultLocation(String.valueOf(location.getLatitude())+','+String.valueOf(location.getLongitude()));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == AppEngine.getInstance().constants.STORAGE_PERMISSION_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            AppEngine.getInstance().androidIntent.startCameraActivity(this, AppEngine.getInstance().constants.MESSAGE_ACTIVITY_RESULT_CODE_CAMERA);
        }
    }

    @OnClick(R.id.signUpBtn)
    public void signUpBtnOnClick() {
        Customer customer = new Customer();
        customer.setAddress(defaultLocation.getText().toString());

        if(gender.getText().toString().equals("Male")) customer.setGender("M");
        else if(gender.getText().toString().equals("Female")) customer.setGender("F");
        else customer.setGender("O");

        customer.setMobileNumber(mobileNumber.getText().toString());
        customer.setNationality(1);

        User user = new User();

        user.setEmail(email.getText().toString());
        user.setName(name.getText().toString());
        user.setPassword(password.getText().toString());
        user.setPicture(null);

        customer.setUser(user);
        customer.setBirthDate(birthDate.getText().toString());

        customerRegistrationPresenter.registerUser(customer);
        showloading();
    }
}
