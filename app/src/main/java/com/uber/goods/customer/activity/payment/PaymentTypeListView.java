package com.uber.goods.customer.activity.payment;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uber.goods.customer.R;
import com.uber.goods.customer.model.payment.PaymentTypeItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kud-wtag on 4/16/18.
 */

public class PaymentTypeListView {

    List<PaymentTypeItem> paymentTypeItems;

    public PaymentTypeListView() {
        paymentTypeItems = new ArrayList<>();
        PaymentTypeItem paymentTypeItem = new PaymentTypeItem();
        paymentTypeItem.setTitle("Cash");
        paymentTypeItem.setResId(R.mipmap.cash);
        paymentTypeItems.add(paymentTypeItem);

        paymentTypeItem = new PaymentTypeItem();
        paymentTypeItem.setTitle("Card");
        paymentTypeItem.setResId(R.mipmap.card);
        paymentTypeItems.add(paymentTypeItem);
    }

    public void setView(final Activity activity, final Context context, LinearLayout listLayout) {
        LayoutInflater inflater = LayoutInflater.from(context);

        for(int i=0; i<paymentTypeItems.size(); i++) {
            View view = null;
            view = inflater.inflate(R.layout.activity_payment_method_item, listLayout, false);

            MenuItemViewHolder viewHolder = new MenuItemViewHolder();
            viewHolder.paymentTypeTitle = view.findViewById(R.id.paymentTypeTitle);
            viewHolder.paymentTypeImg = view.findViewById(R.id.paymentTypeImg);
            view.setTag(viewHolder);

            MenuItemViewHolder holder = (MenuItemViewHolder) view.getTag();
            holder.paymentTypeTitle.setText(paymentTypeItems.get(i).getTitle());

            Drawable buttonIcon = ResourcesCompat.getDrawable(context.getResources(), paymentTypeItems.get(i).getResId(), null);
            buttonIcon.setBounds(0, 0, (int) (buttonIcon.getIntrinsicWidth() * 0.6), (int) (buttonIcon.getIntrinsicHeight() * 0.6));
            ScaleDrawable sd = new ScaleDrawable(buttonIcon, 0, 47, 26);

            holder.paymentTypeImg.setImageDrawable(buttonIcon);

            listLayout.addView(view);
        }
    }
}

class MenuItemViewHolder {
    public TextView paymentTypeTitle;
    public ImageView paymentTypeImg;
}
