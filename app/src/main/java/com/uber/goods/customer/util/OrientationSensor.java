package com.uber.goods.customer.util;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.uber.goods.customer.AppEngine;

/**
 * Created by bitto on 17-Apr-18.
 */

public class OrientationSensor implements SensorEventListener {

    SensorManager mSensorManager;

    float[] mGravity = new float[3];
    float[] mGeomagnetic = new float[3];
    float azimut;
    OrientationSensorListner orientationSensorListner;

    public void init(Activity activity, Context context, OrientationSensorListner orientationSensorListner) {
        mSensorManager = (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), 9000000);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), 9000000);
        this.orientationSensorListner = orientationSensorListner;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = sensorEvent.values;

        if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = sensorEvent.values;

        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];

            if (SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic)) {


                // orientation contains azimut, pitch and roll
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);

                azimut = orientation[0];
                float rotation = -azimut * 360 / (2 * 3.14159f);

                if(rotation >= 0.0f && rotation <= 180.0f){

                }else{
                    rotation = 180 + (180 + rotation);
                }
                rotation = 360.0f - rotation;
                AppEngine.getInstance().constants.ROTATION = rotation;
                orientationSensorListner.onRotationChange(rotation);
            }
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void sensorDetach() {
        mSensorManager.unregisterListener(this);
    }
}
