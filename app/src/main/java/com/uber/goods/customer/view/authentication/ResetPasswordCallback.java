package com.uber.goods.customer.view.authentication;

import com.uber.goods.customer.model.authentication.ResetPasswordRespose;

public interface ResetPasswordCallback {
    public void success(ResetPasswordRespose resetPasswordRespose);
    public void error();
}
