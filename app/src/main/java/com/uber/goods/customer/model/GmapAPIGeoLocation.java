package com.uber.goods.customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kud-wtag on 3/7/18.
 */

public class GmapAPIGeoLocation {
    @SerializedName("formatted_address")
    @Expose
    private String formatted_address;

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }
}
