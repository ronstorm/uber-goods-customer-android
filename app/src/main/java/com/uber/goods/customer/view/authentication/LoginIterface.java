package com.uber.goods.customer.view.authentication;

import com.uber.goods.customer.model.authentication.CustomerLoginResponse;
import com.uber.goods.customer.model.authentication.LoginResponse;
import com.uber.goods.customer.model.user.Customer;

/**
 * Created by Well Travel Android Team on 12/5/17.
 *
 */

public interface LoginIterface {
    <T> void openActivity(Class<T> activityClass);
    void loginResponse(LoginResponse loginResponse);
    void customerLogin(CustomerLoginResponse CustomerLoginResponse);
    void userProfile(Customer customer);
    void error();
}
