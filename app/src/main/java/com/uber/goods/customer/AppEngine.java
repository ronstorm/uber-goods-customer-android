package com.uber.goods.customer;

import com.uber.goods.customer.util.AndroidIntent;
import com.uber.goods.customer.util.Animation;
import com.uber.goods.customer.util.Base64Converter;
import com.uber.goods.customer.util.BlurEditText;
import com.uber.goods.customer.util.CommonUtils;
import com.uber.goods.customer.util.Constants;
import com.uber.goods.customer.util.DatePickerClass;
import com.uber.goods.customer.util.DateTimeHelper;
import com.uber.goods.customer.util.GoogleMapView;
import com.uber.goods.customer.util.ListViewHelper;
import com.uber.goods.customer.util.MenuItemList;
import com.uber.goods.customer.util.NetworkAdapter;
import com.uber.goods.customer.util.NetworkUtils;
import com.uber.goods.customer.util.SVGImage.SVGImageRederer;
import com.uber.goods.customer.util.SharedPrefUtils;
import com.uber.goods.customer.util.SpinnerUtil;
import com.uber.goods.customer.util.StorageImageHelper;
import com.uber.goods.customer.util.TimerUtil;
import com.uber.goods.customer.util.socket.io.SocketIO;

/**
 * Created by anonymous on 12/5/17.
 * Main App Engine
 */

public class AppEngine {
    private static volatile AppEngine ourInstance;
    private static final Object mutex = new Object();

    public final NetworkAdapter networkAdapter;
    public final Constants constants;
    public final TimerUtil timerUtil;
    public final AndroidIntent androidIntent;
    public final BlurEditText blurEditText;
    public final GoogleMapView googleMapView;
    public final SVGImageRederer svgImageRederer;
    public final SpinnerUtil spinnerUtil;
    public final MenuItemList menuItemList;
    public final ListViewHelper listViewHelper;
    public final SharedPrefUtils sharedPrefUtils;
    public final CommonUtils commonUtils;
    public final SocketIO socketIO;
    public final Animation animation;
    public final Base64Converter base64Converter;
    public final DateTimeHelper dateTimeHelper;
    public final NetworkUtils networkUtils;
    public final DatePickerClass datePickerClass;
    public final StorageImageHelper storageImageHelper;

    private AppEngine() {
        networkAdapter = new NetworkAdapter();
        constants = new Constants();
        timerUtil = new TimerUtil();
        androidIntent = new AndroidIntent();
        blurEditText = new BlurEditText();
        googleMapView = new GoogleMapView();
        svgImageRederer = new SVGImageRederer();
        spinnerUtil = new SpinnerUtil();
        menuItemList = new MenuItemList();
        listViewHelper = new ListViewHelper();
        sharedPrefUtils =  new SharedPrefUtils();
        commonUtils = new CommonUtils();
        socketIO = new SocketIO();
        animation = new Animation();
        base64Converter = new Base64Converter();
        dateTimeHelper = new DateTimeHelper();
        networkUtils = new NetworkUtils();
        datePickerClass = new DatePickerClass();
        storageImageHelper = new StorageImageHelper();
    }

    public static AppEngine getInstance() {
        AppEngine result = ourInstance;
        if (result == null) {
            synchronized (mutex) {
                result = ourInstance;
                if (result == null)
                    ourInstance = result = new AppEngine();
            }
        }
        return result;
    }
}
