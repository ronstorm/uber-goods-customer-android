package com.uber.goods.customer.util;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;

/**
 * Created by anonymous on 2/1/18.
 */

public class Animation {

    private float viewAlpha;
    private boolean initFadeing;
    private boolean fading;

    public Animation() {
        viewAlpha = 0;
        initFadeing = false;
    }

    public void slideUp(View view, int duration){
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(duration);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }
    public void slideDown(final View view, int duration){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(duration);
        animate.setFillAfter(true);
        animate.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
            @Override
            public void onAnimationStart(android.view.animation.Animation animation) {

            }

            @Override
            public void onAnimationEnd(android.view.animation.Animation animation) {
                view.setVisibility(View.GONE);
                Log.d("aaaaaaaaaaaaaaa", " gone");
            }

            @Override
            public void onAnimationRepeat(android.view.animation.Animation animation) {

            }
        });
        view.startAnimation(animate);
    }

    public void fadeAway(final Context context, final View view) {
        AppEngine.getInstance().timerUtil.stoptimertask();
        if(initFadeing) {
            fading = true;
            viewAlpha = 1;
            view.setAlpha(1);
            view.setVisibility(View.VISIBLE);
            initFadeing = !initFadeing;
        }
        AppEngine.getInstance().timerUtil.initTimeCustomTask(context, 1, new TImerUtilInterface() {
            @Override
            public void executeTask() {
                if(viewAlpha>0) {
                    view.setAlpha(viewAlpha);
                    viewAlpha -= 0.05;
                    fadeAway(context, view);
                } else {
                    view.setAlpha(0);
                    viewAlpha = 0;
                    initFadeing = false;
                    view.setVisibility(View.INVISIBLE);
                    fading = false;
                    AppEngine.getInstance().timerUtil.stoptimertask();
                }
            }
        });
    }

    public void fadeIn(final Context context, final View view) {
        AppEngine.getInstance().timerUtil.stoptimertask();
        if(initFadeing) {
            fading = true;
            viewAlpha = 0;
            view.setAlpha(0);
            view.setVisibility(View.VISIBLE);
            initFadeing = !initFadeing;
        }
        AppEngine.getInstance().timerUtil.initTimeCustomTask(context, 1, new TImerUtilInterface() {
            @Override
            public void executeTask() {
                if(viewAlpha<1) {
                    view.setAlpha(viewAlpha);
                    viewAlpha += 0.05;
                    fadeIn(context, view);
                } else {
                    view.setAlpha(1);
                    viewAlpha = 1;
                    initFadeing = false;
                    view.setVisibility(View.VISIBLE);
                    fading = false;
                    AppEngine.getInstance().timerUtil.stoptimertask();
                }
            }
        });
    }

    public void expand(final View v, final Context context) {
        final android.view.animation.Animation slideUpAnimation = AnimationUtils.loadAnimation(context,
                R.anim.slide_up);
        slideUpAnimation.setFillAfter(true);
        slideUpAnimation.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
            @Override
            public void onAnimationStart(android.view.animation.Animation animation) {

            }

            @Override
            public void onAnimationEnd(android.view.animation.Animation animation) {
                v.setVisibility(View.VISIBLE);
                slideUpAnimation.setFillAfter(false);
            }

            @Override
            public void onAnimationRepeat(android.view.animation.Animation animation) {

            }
        });
        v.startAnimation(slideUpAnimation);
    }

    public void collapse(final View v, final Context context) {
        final android.view.animation.Animation slideUpAnimation = AnimationUtils.loadAnimation(context,
                R.anim.slide_down);
        slideUpAnimation.setFillAfter(true);
        slideUpAnimation.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
            @Override
            public void onAnimationStart(android.view.animation.Animation animation) {

            }

            @Override
            public void onAnimationEnd(android.view.animation.Animation animation) {
                v.setVisibility(View.INVISIBLE);
                slideUpAnimation.setFillAfter(false);
            }

            @Override
            public void onAnimationRepeat(android.view.animation.Animation animation) {

            }
        });
        v.startAnimation(slideUpAnimation);
    }

    public void setViewAlpha(float viewAlpha) {
        this.viewAlpha = viewAlpha;
    }

    public boolean isFading() {
        return fading;
    }

    public void setFading(boolean fading) {
        this.fading = fading;
    }

    public void setInitFadeing(boolean initFadeing) {
        this.initFadeing = initFadeing;
    }
}

