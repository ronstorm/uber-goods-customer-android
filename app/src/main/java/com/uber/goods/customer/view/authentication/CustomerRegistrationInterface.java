package com.uber.goods.customer.view.authentication;

import com.uber.goods.customer.model.user.Customer;

/**
 * Created by kud-wtag on 3/7/18.
 */

public interface CustomerRegistrationInterface {
    void setDefaultLocation(String Location);
    void registrationSuccess(Customer customer);
    void restrationError();
}
