package com.uber.goods.customer.model.payment;

/**
 * Created by kud-wtag on 4/16/18.
 */

public class CardEntity {
    String cardName;
    String cardNumber;

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
