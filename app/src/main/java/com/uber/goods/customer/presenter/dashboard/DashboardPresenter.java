package com.uber.goods.customer.presenter.dashboard;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.BuildConfig;
import com.uber.goods.customer.activity.mytrips.Rating;
import com.uber.goods.customer.activity.mytrips.RatingResponse;
import com.uber.goods.customer.model.dashboard.PaymentRequest;
import com.uber.goods.customer.model.dashboard.PaymentResponse;
import com.uber.goods.customer.model.dashboard.TripRequest;
import com.uber.goods.customer.model.user.DriverProfile;
import com.uber.goods.customer.network.service.DashboardApiService;
import com.uber.goods.customer.network.service.RequestDeliveryApiService;
import com.uber.goods.customer.network.service.TripRestService;
import com.uber.goods.customer.util.DirectionsJSONParser;
import com.uber.goods.customer.view.dashboard.DashboardInterface;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;

/**
 * Created by bitto on 17-Apr-18.
 */

public class DashboardPresenter {
    private final Context context;
    private final DashboardInterface dashboardInterface;

    public DashboardPresenter(Context context, DashboardInterface dashboardInterface) {
        this.context = context;
        this.dashboardInterface = dashboardInterface;
    }

    public void getDirection(String d) {
        String s = String.valueOf(AppEngine.getInstance().constants.LOCATION.getLatitude())+","+String.valueOf(AppEngine.getInstance().constants.LOCATION.getLongitude());
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.create(DashboardApiService.class, "https://maps.googleapis.com")
                .getAddressFromLatLng(s, d, BuildConfig.GoogleApiKey), new Observer<String>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                Toast.makeText(context, "Logging in....", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onError(@NonNull Throwable e) {
                Toast.makeText(context, "Error Occured", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onComplete() {

            }
            @Override
            public void onNext(@NonNull String data) {
                //https://stackoverflow.com/questions/14710744/how-to-draw-road-directions-between-two-geocodes-in-android-google-map-v2
                List<List<HashMap<String, String>>> routes = null;

                try{
                    //JSONArray jo=jsonObject.getJSONArray("geocoded_waypoints");
                    //System.out.println(jo.getJSONObject(0).getString("geocoder_status"));
                    DirectionsJSONParser parser = new DirectionsJSONParser();

                    // Starts parsing data
                    JSONObject jsonObject = new JSONObject(data);
                    routes = parser.parse(jsonObject);
                    System.out.println(routes.size());
                }catch(Exception e){
                    e.printStackTrace();
                }
                dashboardInterface.setDirection(routes);

            }
        });
    }

    public void getTripDirection(String s, String d) {
        //String s = String.valueOf(AppEngine.getInstance().constants.LOCATION.getLatitude())+","+String.valueOf(AppEngine.getInstance().constants.LOCATION.getLongitude());
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.create(DashboardApiService.class, "https://maps.googleapis.com")
                .getAddressFromLatLng(s, d, BuildConfig.GoogleApiKey), new Observer<String>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                Toast.makeText(context, "Logging in....", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onError(@NonNull Throwable e) {
                Toast.makeText(context, "Error Occured", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onComplete() {

            }
            @Override
            public void onNext(@NonNull String data) {
                //https://stackoverflow.com/questions/14710744/how-to-draw-road-directions-between-two-geocodes-in-android-google-map-v2
                List<List<HashMap<String, String>>> routes = null;

                try{
                    //JSONArray jo=jsonObject.getJSONArray("geocoded_waypoints");
                    //System.out.println(jo.getJSONObject(0).getString("geocoder_status"));
                    DirectionsJSONParser parser = new DirectionsJSONParser();

                    // Starts parsing data
                    JSONObject jsonObject = new JSONObject(data);
                    routes = parser.parse(jsonObject);
                    System.out.println(routes.size());
                }catch(Exception e){
                    e.printStackTrace();
                }
                dashboardInterface.setDirection(routes);

            }
        });
    }

    public void tripPayment(final PaymentRequest paymentRequest) {
        String tripId = AppEngine.getInstance().sharedPrefUtils.getPref("tripId", context);
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(RequestDeliveryApiService.class, context).tripPayment(paymentRequest, tripId),
                new Observer<PaymentResponse>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {

                    }

                    @Override
                    public void onNext(PaymentResponse paymentResponse) {
                        dashboardInterface.tripPaymentSuccess();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                        dashboardInterface.tripPaymentError(paymentRequest);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getDriverProfile(String driverId) {
        Log.d("driverInfo", "driverid "+AppEngine.getInstance().sharedPrefUtils.getPref("driverId", context));
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(TripRestService.class, context).
                        getDriverProfileInfo(driverId),
                new Observer<DriverProfile>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        Toast.makeText(context, "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull DriverProfile userInfo) {
                        Toast.makeText(context, "Driver Profile Fetched", Toast.LENGTH_SHORT).show();
                        dashboardInterface.fetchDriverInfo(userInfo);
                    }
                });
    }

    public void submitRating(Rating rating) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(DashboardApiService.class, context).submitRating(rating),
                new Observer<RatingResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@NonNull RatingResponse ratingResponse) {
                        Toast.makeText(context, "Success", Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void cancelTrip() {

        if(!AppEngine.getInstance().sharedPrefUtils.getPref("tripId", context).equals("")) {
            AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(TripRestService.class, context).cancelTrip(AppEngine.getInstance().sharedPrefUtils.getPref("tripId", context)),
                    new Observer<Response<String>>() {
                        @Override
                        public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                        }

                        @Override
                        public void onNext(@io.reactivex.annotations.NonNull Response<String> stringResponse) {
                            Toast.makeText(context, "Trip Cancled", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                            Toast.makeText(context, "Cancel Error"+e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }

}
