package com.uber.goods.customer.view.dashboard;

import com.uber.goods.customer.model.dashboard.GoodsCategory;
import com.uber.goods.customer.model.dashboard.PaymentRequest;
import com.uber.goods.customer.model.dashboard.TripEstimateResponse;
import com.uber.goods.customer.model.dashboard.TripRequest;
import com.uber.goods.customer.model.user.DriverProfile;

import java.util.List;

public interface RequestRiderInterface {
    public void setGoodsCategory(List<GoodsCategory> goodsCategories);
    public void tripRequestSubmitted(TripRequest tripRequest);
    public void tripRequestSubmitError();
    public void tripEstimationError();
    public void tripEstimationSuccess(TripEstimateResponse tripEstimateResponse);
}
