package com.uber.goods.customer.activity.payment;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.activity.BaseUIController;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by kud-wtag on 4/16/18.
 */

public class PaymentHomeActivity extends BaseUIController {

    LinearLayout paymentMethodList;
    LinearLayout paymentMethodCardList;

    @InjectView(R.id.addCard)
    public LinearLayout addCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.TOPBAR_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_payment_home);
        this.init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        this.setNavBarTitle("Payment");
        paymentMethodList = (LinearLayout) findViewById(R.id.paymentMethodList);
        paymentMethodCardList = (LinearLayout) findViewById(R.id.paymentMethodCardList);
        PaymentTypeListView paymentTypeListView = new PaymentTypeListView();
        paymentTypeListView.setView(this, this, paymentMethodList);
        PaymentCardItemListView paymentCardItemListView = new PaymentCardItemListView();
        paymentCardItemListView.setView(this, this, paymentMethodCardList);
        ButterKnife.inject(this);
    }

    @OnClick(R.id.addCard)
    public void addCardOnclick() {
        AppEngine.getInstance().androidIntent.startActivity(this, PaymentAddCardActivity.class);
    }
}
