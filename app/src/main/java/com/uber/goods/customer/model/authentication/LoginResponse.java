package com.uber.goods.customer.model.authentication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uber.goods.customer.model.user.User;

/**
 * Created by anonymous on 12/14/17.
 *
 */

public class LoginResponse {

    @SerializedName("status_code")
    @Expose
    private int statusCode;

    @SerializedName("user")
    @Expose
    private User user;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
