package com.uber.goods.customer.presenter.issue;

import android.content.Context;
import android.widget.Toast;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.model.issue.IssueCategory;
import com.uber.goods.customer.network.service.IssueApiService;
import com.uber.goods.customer.view.issue.ReportIssueActivityInterface;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by bitto on 13-May-18.
 */

public class ReportIssuePresenter {
    ReportIssueActivityInterface reportIssueActivityInterface;
    Context context;

    public ReportIssuePresenter(ReportIssueActivityInterface reportIssueActivityInterface, Context context) {
        this.reportIssueActivityInterface = reportIssueActivityInterface;
        this.context = context;
    }

    public void getIssueListAndCategory() {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(IssueApiService.class, context).getIssueCategoryAndTopics(),
                new Observer<List<IssueCategory>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<IssueCategory> issueCategories) {
                        reportIssueActivityInterface.onIssueListAndCategoryFetched(issueCategories);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        reportIssueActivityInterface.onIssueListAndCategoryFetchError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
