package com.uber.goods.customer.util;

/**
 * Created by anonymous on 02-Mar-18.
 */

public interface NetworkUtilsInterface {
    public void networkDisconnect();
    public void networkConnect();
}
