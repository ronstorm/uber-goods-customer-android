package com.uber.goods.customer.activity.issue;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.activity.BaseUIController;
import com.uber.goods.customer.model.issue.IssueCreateRequest;
import com.uber.goods.customer.model.issue.IssueCreateResponse;
import com.uber.goods.customer.presenter.issue.ReportSubmitPresenter;
import com.uber.goods.customer.view.issue.ReportIssueSubmitInterface;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by bitto on 16-Apr-18.
 */

public class ReportIssueSubmitActivity extends BaseUIController implements ReportIssueSubmitInterface {

    private ReportSubmitPresenter reportSubmitPresenter;
    private IssueCreateRequest issueCreateRequest;
    @InjectView(R.id.reportIssueField)
    public EditText reportIssueField;
    @InjectView(R.id.cancelButton)
    public Button cancelButton;
    @InjectView(R.id.submitReport)
    public Button submitReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.DRAWER_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_report_problem_form);
        this.init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        ButterKnife.inject(this);
        reportSubmitPresenter = new ReportSubmitPresenter(this, this);
        issueCreateRequest = new IssueCreateRequest();
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            issueCreateRequest.setTopic(Integer.parseInt(bundle.getString("topic")));
        }
    }

    @OnClick(R.id.cancelButton)
    public void onClickCancelButton() {
        finish();
    }
    @OnClick(R.id.submitReport)
    public void onClickSubmitReport() {
        showloading();
        issueCreateRequest.setDetails(reportIssueField.getText().toString());
        reportSubmitPresenter.submitIssue(issueCreateRequest);
    }

    @Override
    public void onSubmitApiSuccess(IssueCreateResponse issueCreateResponse) {
        hideloading();
        finish();
    }

    @Override
    public void onSubmitApiError() {
        hideloading();
    }
}
