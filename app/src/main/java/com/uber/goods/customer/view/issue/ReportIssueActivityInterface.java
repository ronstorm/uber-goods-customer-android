package com.uber.goods.customer.view.issue;

import com.uber.goods.customer.model.issue.IssueCategory;

import java.util.List;

/**
 * Created by bitto on 13-May-18.
 */

public interface ReportIssueActivityInterface {
    void onIssueListAndCategoryFetched(List<IssueCategory> issueCategories);
    void onIssueListAndCategoryFetchError();
}
