package com.uber.goods.customer.util;

/**
 * Created by kud-wtag on 3/4/18.
 */

public interface MenuItemClickListner {
    public void onMenuItemClick();
}
