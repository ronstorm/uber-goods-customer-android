package com.uber.goods.customer.presenter.dashboard;

import android.content.Context;
import android.widget.Toast;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.model.dashboard.GoodsCategory;
import com.uber.goods.customer.model.dashboard.TripEstimate;
import com.uber.goods.customer.model.dashboard.TripEstimateResponse;
import com.uber.goods.customer.model.dashboard.TripRequest;
import com.uber.goods.customer.model.mytrips.TripItem;
import com.uber.goods.customer.model.user.DriverProfile;
import com.uber.goods.customer.network.service.DashboardApiService;
import com.uber.goods.customer.network.service.RequestDeliveryApiService;
import com.uber.goods.customer.network.service.TripRestService;
import com.uber.goods.customer.view.dashboard.RequestRiderInterface;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class RequestRiderPresenter {

    private Context context;
    private RequestRiderInterface requestRiderInterface;

    public RequestRiderPresenter(Context context, RequestRiderInterface requestRiderInterface) {
        this.context = context;
        this.requestRiderInterface = requestRiderInterface;
    }

    public void loadGoodsCategories() {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(RequestDeliveryApiService.class, context).getAllGoodsCategory(),
                new Observer<List<GoodsCategory>>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {

                    }

                    @Override
                    public void onNext(List<GoodsCategory> goodsCategories) {
                        requestRiderInterface.setGoodsCategory(goodsCategories);
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void estimateTrip(TripEstimate tripEstimate) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(RequestDeliveryApiService.class, context).tripEstimation(tripEstimate),
                new Observer<TripEstimateResponse>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {

                    }

                    @Override
                    public void onNext(TripEstimateResponse tripEstimateResponse) {
                        requestRiderInterface.tripEstimationSuccess(tripEstimateResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        requestRiderInterface.tripRequestSubmitError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void submitTripRequest(TripRequest tripRequest) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(RequestDeliveryApiService.class, context).requestTrip(tripRequest),
                new Observer<TripRequest>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull TripRequest tripRequest) {
                        requestRiderInterface.tripRequestSubmitted(tripRequest);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        requestRiderInterface.tripRequestSubmitError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void submitRecurringTripRequest(TripRequest tripRequest) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(RequestDeliveryApiService.class, context).requestRecurringTrip(tripRequest),
                new Observer<TripRequest>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull TripRequest tripRequest) {
                        requestRiderInterface.tripRequestSubmitted(tripRequest);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        requestRiderInterface.tripRequestSubmitError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
