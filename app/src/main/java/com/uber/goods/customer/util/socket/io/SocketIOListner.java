package com.uber.goods.customer.util.socket.io;

import org.json.JSONObject;

/**
 * Created by anonymous on 1/30/18.
 */

public interface SocketIOListner {
    public void onMessageReceive(JSONObject data);
    public void onImageMessageReceive(JSONObject data);
}
