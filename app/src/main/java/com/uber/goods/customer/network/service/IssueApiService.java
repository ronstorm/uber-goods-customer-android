package com.uber.goods.customer.network.service;

import com.uber.goods.customer.model.issue.IssueCategory;
import com.uber.goods.customer.model.issue.IssueCreateRequest;
import com.uber.goods.customer.model.issue.IssueCreateResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by bitto on 12-May-18.
 */

public interface IssueApiService {
    @GET("/api/v1/issue/categories/")
    Observable<List<IssueCategory>> getIssueCategoryAndTopics();

    @POST("/api/v1/issue/")
    Observable<IssueCreateResponse> submitIssue(@Body IssueCreateRequest issueCreateRequest);
}
