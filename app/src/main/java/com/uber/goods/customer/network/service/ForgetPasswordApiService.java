package com.uber.goods.customer.network.service;

import com.uber.goods.customer.model.authentication.ForgetPassword;
import com.uber.goods.customer.model.authentication.ForgetPasswordResponse;
import com.uber.goods.customer.model.authentication.ResetPassword;
import com.uber.goods.customer.model.authentication.ResetPasswordRespose;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ForgetPasswordApiService {

    @POST("/api/v1/account/forgot-password/")
    Observable<Response<String>> forgetPassword(@Body ForgetPassword forgetPassword);

    @POST("/api/v1/account/reset-password/")
    Observable<ResetPasswordRespose> resetPassword(@Body ResetPassword resetPassword);
}
