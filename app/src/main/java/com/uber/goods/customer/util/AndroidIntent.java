package com.uber.goods.customer.util;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

/**
 * Created by anonymous on 12/13/17.
 * Android Intent Util
 */

public class AndroidIntent {
    Uri imageUri;
    public AndroidIntent() {

    }
    public <T> void startActivity(Context context, Class<T> className) {
        Intent intent = new Intent(context, className);
        context.startActivity(intent);
    }
    public void startCameraActivity(Activity activity, int RESULT_ID) {
        //Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()));
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = activity.getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        activity.startActivityForResult(intent, RESULT_ID);
    }
    public void startFileSelectActivity(Activity activity, int RESULT_ID) {
        Intent intent = new Intent().setType("*/*").setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(Intent.createChooser(intent, "Select a file"), RESULT_ID);
    }

    public Uri getImageUri() {
        return imageUri;
    }
}
