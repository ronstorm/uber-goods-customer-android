package com.uber.goods.customer.util;

import android.location.Location;

/**
 * Created by Well Travel Android Team on 12/5/17.
 * Constant Values
 */

public class Constants {
    public final String SHARED_PREF = "uberGoodsCurtomerKeys";
    public final String GOOGLE_MAP_GEO_CODING_BASE_URL = "https://maps.googleapis.com";


    public final String EMAIL_PATTERN = "[a-zA-Z0-9_!#$%&'*+/=?`{|}~^]+[.-]?[a-zA-Z0-9]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+$";
    public final String PHONENUMBER_PATTERN = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
    public final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
    public final int LOGIN_EMPTY_FIELD = 1;

    //layout type
    public final String DRAWER_LAYOUT = "drawerLayout";
    public final String TOPBAR_LAYOUT = "topBarLayout";
    public final String EMPTY_LAYOUT = "emptyLayout";

    //message Activity
    public final String MESSAGE_TYPE_TEXT = "message";
    public final String MESSAGE_TYPE_IMAGE = "image";
    public final int MESSAGE_ACTIVITY_RESULT_CODE_CAMERA = 1231;
    public final int MESSAGE_ACTIVITY_RESULT_CODE_FILE = 123;
    public final String MESSAGE_DATA_TYPE_URI = "uri";
    public final String MESSAGE_DATA_TYPE_BITMAP = "bitmap";
    public final String MESSAGE_ATTR_USER = "user";
    public final String MESSAGE_ATTR_SMS = "sms";
    public final String MESSAGE_ATTR_IMAGE = "imageData";
    public final String MESSAGE_SOCKET_IO_HOST = "tbd";
    public final String INTENT_DATA_IMAGE = "data";
    public String CURRENT_LOCATION = "";
    //message Activity End

    public String userImageLink;
    public String clientName;

    //Custmer Registration
    public final int STORAGE_PERMISSION_CODE = 875;
    //customer Registration end

    public float ROTATION = 0.0f;
    public Location LOCATION;

    //OAuth
    public final String OAUTH_CLIENT_ID = "FwzZFbyFK0u1u6UNCFRCydm6jMuAC13zbWKWt9Hw";
    public final String OAUTH_CLIENT_SECRET = "fceFVxfLtM6mvgKmJwFQNBhHnxjCyYwMw8y8cBVIC4PgAsrwhYFNTHxcDC5H7GW6RViSv317r0eSEogvzi5J16V040pNAjS8OWwchz3c5yD6LSUCJTFoUePcv6sNDWTs";
    public String OAUTH_ACCESS_TOKEN = "access_token";
    public String OAUTH_EXPIRES_IN = "expires_in";
    public String OAUTH_TOKEN_TYPE = "token_type";
    public String OAUTH_REFRESHT_GRANT_TYPE = "refresh_token";
    public String OAUTH_SCOPE = "scope";
    public String OAUTH_REFRESH_TOKEN = "refresh_token";
    public boolean loggenIn = false;
    //OAuth Ends

    //forget password
    public Integer forgetCode = null;
    public String forgetEmail = "";
}
