package com.uber.goods.customer.activity.authentication;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.activity.BaseUIController;

/**
 * Created by bitto on 14-Mar-18.
 */

public class ForgetPasswordMainActivity extends BaseUIController implements View.OnClickListener {

    private ForgetPasswordViewPagerAdapter forgetPasswordViewPagerAdapter;
    boolean initActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.EMPTY_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.forget_password_view_pager);

        this.initActivity = true;
        this.selectFrag(0);
        forgetPasswordViewPagerAdapter = new ForgetPasswordViewPagerAdapter(getSupportFragmentManager());
    }

    public void selectFrag(int id) {
        Fragment fr;
        if(id == 0) {
            fr = new ForgetPasswordRecoveryFragment().setContext(this, this);
        } else if(id == 1) {
            fr = new ForgetPasswordConfirmFragment().setContext(this, this);
        } else if(id == 2) {
            fr = new ForgetPasswordNewPasswordFragment().setContext(this, this);
        } else {
            fr = new ForgetPasswordRecoveryFragment().setContext(this, this);
        }
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        if(this.initActivity) {
            fragmentTransaction.replace(R.id.viewPager, fr);
            this.initActivity = false;
        } else {
            fragmentTransaction.setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_right, R.animator.sline_in_right, R.animator.slide_out_left  );
            fragmentTransaction.replace(R.id.viewPager, fr).addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View view) {
        this.onBackPressed();
    }
}
