package com.uber.goods.customer.activity.authentication;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.model.authentication.ResetPassword;
import com.uber.goods.customer.model.authentication.ResetPasswordRespose;
import com.uber.goods.customer.presenter.authentication.ForgetPasswordPresenter;
import com.uber.goods.customer.view.authentication.ResetPasswordCallback;

/**
 * Created by bitto on 15-Mar-18.
 */

public class ForgetPasswordNewPasswordFragment extends Fragment implements View.OnClickListener, ResetPasswordCallback {

    View view;
    Context context;
    Button nextBtn;
    EditText newPassword;
    EditText confirmPassword;
    ForgetPasswordMainActivity forgetPasswordMainActivity;
    ForgetPasswordPresenter forgetPasswordPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_new_password, container, false);
        this.init();
        return view;
    }

    public void init(){
        nextBtn = view.findViewById(R.id.nextBtnDone);
        newPassword = view.findViewById(R.id.newPassword);
        confirmPassword = view.findViewById(R.id.confirmPassword);
        forgetPasswordPresenter = new ForgetPasswordPresenter(context, this);
        nextBtn.setOnClickListener(this);
    }

    public ForgetPasswordNewPasswordFragment setContext(Context context, ForgetPasswordMainActivity forgetPasswordMainActivity) {
        this.context = context;
        this.forgetPasswordMainActivity = forgetPasswordMainActivity;
        return this;
    }

    @Override
    public void onClick(View view) {
        if(view.equals(nextBtn)) {
            if(!newPassword.getText().toString().equals("") && !confirmPassword.getText().toString().equals("")) {

                if(newPassword.getText().toString().equals(confirmPassword.getText().toString())) {
                    forgetPasswordMainActivity.showloading();
                    ResetPassword resetPassword = new ResetPassword();
                    resetPassword.setCode(AppEngine.getInstance().constants.forgetCode);
                    resetPassword.setEmail(AppEngine.getInstance().constants.forgetEmail);
                    resetPassword.setNewPassword(newPassword.getText().toString());
                    forgetPasswordPresenter.resetPassword(resetPassword);
                } else {
                    Toast.makeText(context, "Passwords Do not Match", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Empty Fields", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void success(ResetPasswordRespose resetPasswordRespose) {
        forgetPasswordMainActivity.hideloading();
        Toast.makeText(context, "Password Reset SuccessFull", Toast.LENGTH_SHORT).show();
        forgetPasswordMainActivity.finish();
    }

    @Override
    public void error() {
        forgetPasswordMainActivity.hideloading();
        Toast.makeText(context, "Error Occured", Toast.LENGTH_SHORT).show();
    }
}
