package com.uber.goods.customer;

import android.app.Activity;
import android.app.Application;
import android.content.IntentFilter;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;

import com.uber.goods.customer.util.GoogleMapView;
import com.uber.goods.customer.util.LocationChangeListner;

/**
 * Created by anonymous on 12/5/17.
 *
 */

public class MainApplication extends Application implements  Application.ActivityLifecycleCallbacks, LocationChangeListner {

    public MainApplication() {
        GoogleMapView.dialog = false;
        AppEngine.getInstance().googleMapView.setLocationChangeListner(this);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        this.registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(AppEngine.getInstance().networkUtils, filter);
        AppEngine.getInstance().googleMapView.refreshLocManager(activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        //AppEngine.getInstance().googleMapView.detachMap();
    }

    @Override
    public void onLocationChangedApp(Location location) {
        AppEngine.getInstance().constants.CURRENT_LOCATION = location.getLatitude()+","+location.getLongitude();
        AppEngine.getInstance().constants.LOCATION = location;
    }
}
