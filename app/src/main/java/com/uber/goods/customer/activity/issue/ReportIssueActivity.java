package com.uber.goods.customer.activity.issue;

import android.Manifest;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.LinearLayout;
import com.google.android.gms.maps.MapView;
import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.activity.BaseUIController;
import com.uber.goods.customer.model.issue.IssueCategory;
import com.uber.goods.customer.presenter.issue.ReportIssuePresenter;
import com.uber.goods.customer.util.GmapView;
import com.uber.goods.customer.view.issue.ReportIssueActivityInterface;
import java.util.List;
import butterknife.ButterKnife;

/**
 * Created by bitto on 10-Apr-18.
 */

public class ReportIssueActivity extends BaseUIController  implements ReportIssueActivityInterface {

    private GmapView gmapView;
    private ReportIssuePresenter reportIssuePresenter;
    private ReportIssueMainItemView reportIssueMainItemView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.TOPBAR_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_report_problem_main);
        this.init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {

        this.setNavBarTitle("");

        reportIssueMainItemView = new ReportIssueMainItemView();

        ButterKnife.inject(this);
        gmapView = new GmapView();

        gmapView.setMapCameraOffset(false);
        gmapView.setLoadCustomMarker(false);
        gmapView.initView(this, (MapView) findViewById(R.id.mapView), savedInstanceState, true);
        gmapView.getMapView().onStart();
        AppEngine.getInstance().googleMapView.setGoogleMapLocationListner(this.gmapView);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        reportIssuePresenter = new ReportIssuePresenter(this, this);
        reportIssuePresenter.getIssueListAndCategory();
        showloading();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        AppEngine.getInstance().googleMapView.requestPermissionCallback(requestCode, grantResults, this);
    }

    @Override
    public void onIssueListAndCategoryFetched(List<IssueCategory> issueCategories) {
        reportIssueMainItemView.setView(this, this, (LinearLayout) findViewById(R.id.reportListWrapper), issueCategories);
        hideloading();
    }

    @Override
    public void onIssueListAndCategoryFetchError() {
        hideloading();
    }
}
