package com.uber.goods.customer.util;

import android.location.Location;

/**
 * Created by kud-wtag on 3/8/18.
 */

public interface LocationChangeListner {
    void onLocationChangedApp(Location location);
}
