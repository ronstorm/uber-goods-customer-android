package com.uber.goods.customer.network.service;

import com.uber.goods.customer.model.mytrips.TripItem;
import com.uber.goods.customer.model.user.DriverProfile;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import io.reactivex.Observable;
import retrofit2.http.Path;

/**
 * Created by kud-wtag on 6/24/18.
 */

public interface TripRestService {
    @GET("/api/v1/trip/")
    Observable<List<TripItem>> getAllTrips();

    @GET("/api/v1/recurring-trip/")
    Observable<List<TripItem>> getAllRecurringTrips();

    @GET("/api/v1/driver/{id}/")
    Observable<DriverProfile> getDriverProfileInfo(@Path("id") String id);

    @DELETE("/api/v1/trip/{id}/")
    Observable<Response<String>> cancelTrip(@Path("id") String id);
}
