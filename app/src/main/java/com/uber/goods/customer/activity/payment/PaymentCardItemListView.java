package com.uber.goods.customer.activity.payment;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uber.goods.customer.R;
import com.uber.goods.customer.model.payment.CardEntity;
import com.uber.goods.customer.model.payment.PaymentTypeItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kud-wtag on 4/16/18.
 */

public class PaymentCardItemListView {

    List<CardEntity> cardEntities;

    public PaymentCardItemListView() {
        cardEntities = new ArrayList<>();
        CardEntity cardEntity = new CardEntity();
        cardEntity.setCardName("Active Card");
        cardEntity.setCardNumber("*** *** 232");
        cardEntities.add(cardEntity);
    }

    public void setView(final Activity activity, final Context context, LinearLayout listLayout) {
        LayoutInflater inflater = LayoutInflater.from(context);
        for(int i=0; i<this.cardEntities.size(); i++) {
            View view = null;
            view = inflater.inflate(R.layout.activity_payment_method_card_list_item, listLayout, false);

            ItemHolder viewHolder = new ItemHolder();
            viewHolder.cardName = view.findViewById(R.id.cardName);
            viewHolder.cardNumber = view.findViewById(R.id.cardNumber);
            viewHolder.deleteCardBtn = view.findViewById(R.id.deleteCardBtn);
            view.setTag(viewHolder);

            ItemHolder holder = (ItemHolder) view.getTag();
            holder.cardName.setText(cardEntities.get(i).getCardName());
            holder.cardNumber.setText(cardEntities.get(i).getCardNumber());

            listLayout.addView(view);
        }
    }
}

class ItemHolder {
    public TextView cardName;
    public TextView cardNumber;
    public ImageView deleteCardBtn;
}
