package com.uber.goods.customer.model.payment;

/**
 * Created by kud-wtag on 4/16/18.
 */

public class PaymentTypeItem {
    int resId;
    String title;

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
