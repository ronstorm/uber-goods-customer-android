package com.uber.goods.customer.activity.payment;

import android.os.Bundle;
import android.widget.LinearLayout;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.activity.BaseUIController;

/**
 * Created by kud-wtag on 4/16/18.
 */

public class PaymentAddCardActivity extends BaseUIController {
    LinearLayout paymentMethodCardList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.TOPBAR_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_payment_method_add_card);
        this.init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        this.setNavBarTitle("Payment");
        paymentMethodCardList = (LinearLayout) findViewById(R.id.paymentMethodCardList);
        PaymentCardItemListView paymentCardItemListView = new PaymentCardItemListView();
        paymentCardItemListView.setView(this, this, paymentMethodCardList);

    }
}
