package com.uber.goods.customer.view.issue;

import com.uber.goods.customer.model.issue.IssueCreateResponse;

/**
 * Created by bitto on 13-May-18.
 */

public interface ReportIssueSubmitInterface {
    void onSubmitApiSuccess(IssueCreateResponse issueCreateResponse);
    void onSubmitApiError();
}
