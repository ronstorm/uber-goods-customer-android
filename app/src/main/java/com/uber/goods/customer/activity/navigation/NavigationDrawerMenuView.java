package com.uber.goods.customer.activity.navigation;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;

/**
 * Created by kud-wtag on 1/9/18.
 */

public class NavigationDrawerMenuView {

    public void setView(final Activity activity, final Context context, LinearLayout menuList, final DrawerLayout drawerLayout) {
        LayoutInflater inflater = LayoutInflater.from(context);
        for (int i=0; i<AppEngine.getInstance().menuItemList.getMenuList(context, activity).size(); i++) {
            View view  = null;
            if(AppEngine.getInstance().menuItemList.getMenuList(context, activity).get(i).getLayoutType()==1) {
                view = inflater.inflate(R.layout.activity_main_navigation_menu_item, menuList, false);

                MenuItemViewHolder viewHolder = new MenuItemViewHolder();

                viewHolder.button = view.findViewById(R.id.navMenuButton);
                viewHolder.navButtonIcon = view.findViewById(R.id.navButtonIcon);
                view.setTag(viewHolder);

                MenuItemViewHolder holder = (MenuItemViewHolder) view.getTag();
                holder.button.setText(AppEngine.getInstance().menuItemList.getMenuList(context, activity).get(i).getTitle());


                Drawable buttonIcon = ResourcesCompat.getDrawable(context.getResources(), AppEngine.getInstance().menuItemList.getMenuList(context, activity).get(i).getImageResId(), null);
                buttonIcon.setBounds(0, 0, (int) (buttonIcon.getIntrinsicWidth() * 0.6), (int) (buttonIcon.getIntrinsicHeight() * 0.6));
                ScaleDrawable sd = new ScaleDrawable(buttonIcon, 0, 40, 40);

                holder.navButtonIcon.setImageDrawable(buttonIcon);
                final int finalI = i;
                holder.button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AppEngine.getInstance().androidIntent.startActivity(context, AppEngine.getInstance().menuItemList.getMenuList(context, activity).get(finalI).getActivityClass());
                        drawerLayout.closeDrawers();
                    }
                });
            }
            else if(AppEngine.getInstance().menuItemList.getMenuList(context, activity).get(i).getLayoutType()==2) {
                view = inflater.inflate(R.layout.avtivity_main_navigation_menu_item_no_icon, menuList, false);

                MenuItemViewHolderNoIcon viewHolder = new MenuItemViewHolderNoIcon();

                viewHolder.button = view.findViewById(R.id.navMenuButton);
                view.setTag(viewHolder);

                MenuItemViewHolderNoIcon holder = (MenuItemViewHolderNoIcon) view.getTag();
                holder.button.setText(AppEngine.getInstance().menuItemList.getMenuList(context, activity).get(i).getTitle());

                final int finalI = i;
                holder.button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    if (AppEngine.getInstance().menuItemList.getMenuList(context, activity).get(finalI).getActivityClass()!=null) {
                        AppEngine.getInstance().androidIntent.startActivity(context, AppEngine.getInstance().menuItemList.getMenuList(context, activity).get(finalI).getActivityClass());
                    }
                    else {
                        AppEngine.getInstance().menuItemList.getMenuList(context, activity).get(finalI).getMenuItemClickListner().onMenuItemClick();
                    }
                    drawerLayout.closeDrawers();
                    }
                });

            }
            else if(AppEngine.getInstance().menuItemList.getMenuList(context, activity).get(i).getLayoutType()==3) {
                view = inflater.inflate(R.layout.activity_main_navigation_item_bar, menuList, false);
            }

            menuList.addView(view);

        }
    }

}

class MenuItemViewHolder {
    public Button button;
    public ImageView navButtonIcon;
}

class MenuItemViewHolderNoIcon {
    public Button button;
}

