package com.uber.goods.customer.network.service;

import com.uber.goods.customer.activity.mytrips.Rating;
import com.uber.goods.customer.activity.mytrips.RatingResponse;
import com.uber.goods.customer.model.GeoLocationResponse;
import com.uber.goods.customer.model.dashboard.GoodsCategory;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by bitto on 17-Apr-18.
 */

public interface DashboardApiService {
    @GET("/maps/api/directions/json")
    Observable<String> getAddressFromLatLng(@Query("origin") String origin, @Query("destination") String destination, @Query("key") String key);

    @POST("/api/v1/rating/")
    Observable<RatingResponse> submitRating(@Body Rating rating);
}
