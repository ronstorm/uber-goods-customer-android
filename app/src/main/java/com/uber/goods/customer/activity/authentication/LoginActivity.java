package com.uber.goods.customer.activity.authentication;

import android.Manifest;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.MapView;
import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.activity.BaseUIController;
import com.uber.goods.customer.activity.dashboard.DashBoardActivity;
import com.uber.goods.customer.model.authentication.CustomerLoginResponse;
import com.uber.goods.customer.model.authentication.LoginResponse;
import com.uber.goods.customer.model.user.Customer;
import com.uber.goods.customer.presenter.authentication.LoginPresenter;
import com.uber.goods.customer.util.GmapView;
import com.uber.goods.customer.util.GoogleMapLocationListner;
import com.uber.goods.customer.util.GoogleMapView;
import com.uber.goods.customer.util.firebase.FireBaseInstanceIdService;
import com.uber.goods.customer.view.authentication.LoginIterface;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class LoginActivity extends BaseUIController implements LoginIterface, View.OnClickListener {

    private LoginPresenter loginPresenter;
    private LinearLayout mainOverlay;

    GmapView gmapView;

    @InjectView(R.id.signUp)
    Button signUp;

    @InjectView(R.id.signInBtn)
    Button signInBtn;

    @InjectView(R.id.forgetPassword)
    Button forgetPassword;

    @InjectView(R.id.email)
    EditText email;

    @InjectView(R.id.password)
    EditText password;

    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.EMPTY_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.authentication_login_coreapp);
        this.init(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEngine.getInstance().googleMapView.setGoogleMapLocationListner(this.gmapView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void init(Bundle savedInstanceState) {
        bundle = getIntent().getExtras();
        loginPresenter = new LoginPresenter(this, this);
        this.mainOverlay = (LinearLayout) this.findViewById(R.id.mainOverlay);
        this.mainOverlay.setOnClickListener(this);
        ButterKnife.inject(this);
        gmapView = new GmapView();

        gmapView.setMapCameraOffset(true);
        gmapView.setLoadCustomMarker(false);
        gmapView.initView(this, (MapView) findViewById(R.id.mapView), savedInstanceState, true);
        gmapView.getMapView().onStart();
        AppEngine.getInstance().googleMapView.setGoogleMapLocationListner(this.gmapView);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(AppEngine.getInstance().constants.loggenIn) {
            activityToOpen();
        }
        else if(!AppEngine.getInstance().sharedPrefUtils.getPref(AppEngine.getInstance().constants.OAUTH_ACCESS_TOKEN, this).equals("")) {
            GoogleMapView.dialog = true;
            AppEngine.getInstance().googleMapView.requestPermissionCallback(requestCode, grantResults, this);
            showloading();
            loginPresenter.loadCustomerProfile();
        } else {
            AppEngine.getInstance().googleMapView.requestPermissionCallback(requestCode, grantResults, this);
        }
    }

    @Override
    public <T> void openActivity(Class <T> activityClass) {

    }

    @Override
    public void loginResponse(LoginResponse loginResponse) {
        hideloading();
        //AppEngine.getInstance().androidIntent.startActivity(this, DashBoardActivity.class);
        AppEngine.getInstance().constants.loggenIn = true;
        activityToOpen();
    }

    @Override
    public void customerLogin(CustomerLoginResponse customerLoginResponse) {
        Toast.makeText(this, customerLoginResponse.getAccessToken(), Toast.LENGTH_LONG).show();
        AppEngine.getInstance().constants.loggenIn = true;
        FireBaseInstanceIdService fireBaseInstanceIdService = new FireBaseInstanceIdService();
        fireBaseInstanceIdService.registerFcmToken(this);
        loginPresenter.loadCustomerProfile();
    }

    @Override
    public void userProfile(Customer customer) {
        GoogleMapView.dialog = false;
        hideloading();
        AppEngine.getInstance().constants.clientName = customer.getUser().getName();
        AppEngine.getInstance().constants.userImageLink = customer.getUser().getPicture();
        //AppEngine.getInstance().androidIntent.startActivity(this, DashBoardActivity.class);
        AppEngine.getInstance().constants.loggenIn = true;
        activityToOpen();
    }

    @Override
    public void error() {
        hideloading();
    }

    @Override
    public void onClick(View view) {
        if(view.equals(this.mainOverlay)) {

        }
    }

    @OnClick(R.id.signUp)
    public void onSignUpClick() {
        AppEngine.getInstance().androidIntent.startActivity(this, CustomerRegistrationActivity.class);
    }

    @OnClick(R.id.signInBtn)
    public void signInBtnClick() {
        showloading();
        this.loginPresenter.customerLogin(email.getText().toString(), password.getText().toString());
    }

    @OnClick(R.id.forgetPassword)
    public void forgetPasswordClick() {
        AppEngine.getInstance().androidIntent.startActivity(this, ForgetPasswordMainActivity.class);
    }

    private void activityToOpen() {
        if(bundle!=null) {
            Intent intent = new Intent(this, DashBoardActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            AppEngine.getInstance().androidIntent.startActivity(this, DashBoardActivity.class);
        }
    }
}
