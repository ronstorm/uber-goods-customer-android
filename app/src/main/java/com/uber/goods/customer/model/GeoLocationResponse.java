package com.uber.goods.customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kud-wtag on 3/7/18.
 */
public class GeoLocationResponse {
    @SerializedName("results")
    @Expose
    private List<GmapAPIGeoLocation> results;
    @SerializedName("status")
    @Expose
    private String status;

    public List<GmapAPIGeoLocation> getResults() {
        return results;
    }

    public void setResults(List<GmapAPIGeoLocation> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
