package com.uber.goods.customer.model.user;

import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.widget.EditText;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kud-wtag on 6/24/18.
 */

public class DriverProfile {

    @SerializedName("id")
    private int id;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("birth_date")
    @Expose
    private String birthDate;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("nationality")
    @Expose
    private Integer nationality;
    @SerializedName("driving_license_no")
    @Expose
    private String drivingLicenseNo;
    @SerializedName("driving_license_renewal_date")
    @Expose
    private String drivingLicenseRenewalDate;
    @SerializedName("passport_number")
    @Expose
    private String passportNumber;
    @SerializedName("passport_renewal_date")
    @Expose
    private String passportRenewalDate;
    @SerializedName("work_permit_number")
    @Expose
    private String workPermitNumber;
    @SerializedName("area_of_interest")
    @Expose
    private String areaOfInterest;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("profile_type")
    @Expose
    private String profileType;

    private DriverProfile driverProfile;

    public DriverProfile(){

    }

    public DriverProfile(User user, String gender, String birthDate, String mobileNumber, Integer nationality, String drivingLicenseNo, String drivingLicenseRenewalDate, String passportNumber, String passportRenewalDate, String workPermitNumber, String areaOfInterest, String address, String companyName, String profileType) {
        this.user = user;
        this.gender = gender;
        this.birthDate = birthDate;
        this.mobileNumber = mobileNumber;
        this.nationality = nationality;
        this.drivingLicenseNo = drivingLicenseNo;
        this.drivingLicenseRenewalDate = drivingLicenseRenewalDate;
        this.passportNumber = passportNumber;
        this.passportRenewalDate = passportRenewalDate;
        this.workPermitNumber = workPermitNumber;
        this.areaOfInterest = areaOfInterest;
        this.address = address;
        this.companyName = companyName;
        this.profileType = profileType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Integer getNationality() {
        return nationality;
    }

    public void setNationality(Integer nationality) {
        this.nationality = nationality;
    }

    public String getDrivingLicenseNo() {
        return drivingLicenseNo;
    }

    public void setDrivingLicenseNo(String drivingLicenseNo) {
        this.drivingLicenseNo = drivingLicenseNo;
    }

    public String getDrivingLicenseRenewalDate() {
        return drivingLicenseRenewalDate;
    }

    public void setDrivingLicenseRenewalDate(String drivingLicenseRenewalDate) {
        this.drivingLicenseRenewalDate = drivingLicenseRenewalDate;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportRenewalDate() {
        return passportRenewalDate;
    }

    public void setPassportRenewalDate(String passportRenewalDate) {
        this.passportRenewalDate = passportRenewalDate;
    }

    public String getWorkPermitNumber() {
        return workPermitNumber;
    }

    public void setWorkPermitNumber(String workPermitNumber) {
        this.workPermitNumber = workPermitNumber;
    }

    public String getAreaOfInterest() {
        return areaOfInterest;
    }

    public void setAreaOfInterest(String areaOfInterest) {
        this.areaOfInterest = areaOfInterest;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    @BindingAdapter("android:text")
    public static void setText(EditText view, int value) {
        view.setText(Integer.toString(value));
    }

    @InverseBindingAdapter(attribute = "android:text")
    public static int getText(EditText view) {
        return Integer.parseInt(view.getText().toString());
    }


    @Override
    public String toString() {
        return "UserInfo{" +
                "user=" + user +
                ", gender='" + gender + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", nationality=" + nationality +
                ", drivingLicenseNo='" + drivingLicenseNo + '\'' +
                ", drivingLicenseRenewalDate='" + drivingLicenseRenewalDate + '\'' +
                ", passportNumber='" + passportNumber + '\'' +
                ", passportRenewalDate='" + passportRenewalDate + '\'' +
                ", workPermitNumber='" + workPermitNumber + '\'' +
                ", areaOfInterest='" + areaOfInterest + '\'' +
                ", address='" + address + '\'' +
                ", companyName='" + companyName + '\'' +
                ", profileType='" + profileType + '\'' +
                '}';
    }


}
