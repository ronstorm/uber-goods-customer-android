package com.uber.goods.customer.view.mytrip;

import com.uber.goods.customer.model.mytrips.TripItem;

import java.util.List;

/**
 * Created by kud-wtag on 6/24/18.
 */

public interface MytripsRecurringInterface {
    void tripsFetchSuccess(List<TripItem> tripItemList);
    void tripsFetchError();
}
