package com.uber.goods.customer.presenter.authentication;

import android.content.Context;
import android.widget.Toast;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.model.authentication.ForgetPassword;
import com.uber.goods.customer.model.authentication.ForgetPasswordResponse;
import com.uber.goods.customer.model.authentication.ResetPassword;
import com.uber.goods.customer.model.authentication.ResetPasswordRespose;
import com.uber.goods.customer.network.service.ForgetPasswordApiService;
import com.uber.goods.customer.util.NetworkAdapter;
import com.uber.goods.customer.view.authentication.ForgetPasswordCallback;
import com.uber.goods.customer.view.authentication.ResetPasswordCallback;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;

public class ForgetPasswordPresenter {

    private Context context;
    private ForgetPasswordCallback forgetPasswordCallback;
    private ResetPasswordCallback resetPasswordCallback;
    private NetworkAdapter networkAdapter;

    public ForgetPasswordPresenter(Context context, ForgetPasswordCallback forgetPasswordCallback) {
        this.context = context;
        this.forgetPasswordCallback = forgetPasswordCallback;
        this.networkAdapter = AppEngine.getInstance().networkAdapter;
    }

    public ForgetPasswordPresenter(Context context, ResetPasswordCallback resetPasswordCallback) {
        this.context = context;
        this.resetPasswordCallback = resetPasswordCallback;
        this.networkAdapter = AppEngine.getInstance().networkAdapter;
    }

    public void forgetPassword(final ForgetPassword forgetPassword) {
        networkAdapter.subscriber(networkAdapter.callAPI(ForgetPasswordApiService.class, context).forgetPassword(forgetPassword),
                new Observer<Response<String>>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {

                    }

                    @Override
                    public void onNext(Response<String> forgetPasswordResponse) {
                        if(forgetPasswordResponse.code()==200) {
                            forgetPasswordCallback.success();
                        } else {
                            forgetPasswordCallback.error();
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        //forgetPasswordCallback.error();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void resetPassword(ResetPassword resetPassword) {
        networkAdapter.subscriber(networkAdapter.callAPI(ForgetPasswordApiService.class, context).resetPassword(resetPassword),
                new Observer<ResetPasswordRespose>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {

                    }

                    @Override
                    public void onNext(ResetPasswordRespose resetPasswordRespose) {
                        resetPasswordCallback.success(resetPasswordRespose);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        resetPasswordCallback.error();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
