package com.uber.goods.customer.notification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.content.Intent;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.activity.dashboard.DashBoardActivity;
import com.uber.goods.customer.activity.issue.ReportIssueActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bitto on 15-May-18.
 */

public class DriverFoundNotification extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage message) {
        LocalBroadcastManager broadcaster = LocalBroadcastManager.getInstance(getBaseContext());
        Intent refreshIntent = new Intent("NotificationReceived");
        if(Integer.parseInt(message.getData().get("id"))==1) {
            refreshIntent.putExtra("tripState", 1);
            AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "deliveryAccepted", getApplicationContext());
        } else if(Integer.parseInt(message.getData().get("id"))==2) {
            refreshIntent.putExtra("tripState", 2);
            AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "onTheWay", getApplicationContext());
        } else if(Integer.parseInt(message.getData().get("id"))==3) {
            refreshIntent.putExtra("tripState", 3);
            AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "onTheWayDelivery", getApplicationContext());
        } else if(Integer.parseInt(message.getData().get("id"))==4) {
            refreshIntent.putExtra("tripState", 4);
            AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "tripEnded", getApplicationContext());
        } else if(Integer.parseInt(message.getData().get("id"))==5) {
            refreshIntent.putExtra("tripState", 5);
            AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "", getApplicationContext());
        }

        Gson gson = new Gson();
        Log.d("notificationPayload", gson.toJson(message.getData()));

        refreshIntent.putExtra("id", message.getData().get("id"));

        if(Integer.parseInt(message.getData().get("id"))==1 || Integer.parseInt(message.getData().get("id"))==2 || Integer.parseInt(message.getData().get("id"))==3 || Integer.parseInt(message.getData().get("id"))==4) {
            AppEngine.getInstance().sharedPrefUtils.putPref("tripId", message.getData().get("tripId"), getApplicationContext());
            AppEngine.getInstance().sharedPrefUtils.putPref("driverId", message.getData().get("driverId"), getApplicationContext());
            AppEngine.getInstance().sharedPrefUtils.putPref("pickUp", message.getData().get("pickUp"), getApplicationContext());
            AppEngine.getInstance().sharedPrefUtils.putPref("destination", message.getData().get("destination"), getApplicationContext());
            AppEngine.getInstance().sharedPrefUtils.putPref("total_fare", message.getData().get("total_fare"), getApplicationContext());
        }
        if(Integer.parseInt(message.getData().get("id"))!=8) {
            broadcaster.sendBroadcast(refreshIntent);
            sendMyNotification(message.getNotification().getBody(), message.getData());
        }
        if(Integer.parseInt(message.getData().get("id"))==8) {
            refreshIntent.putExtra("location", message.getData().get("location"));
            refreshIntent.putExtra("rotation", message.getData().get("rotation"));
            refreshIntent.putExtra("bearing", message.getData().get("bearing"));
            broadcaster.sendBroadcast(refreshIntent);
        }
    }


    private void sendMyNotification(String message, Map<String, String> body) {

        //On click of notification it redirect to this Activity
        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, DashBoardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtras(bundle);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel(notificationManager);
        }

        Uri soundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "Uber Goods")
                .setSmallIcon(R.mipmap.logo)
                .setContentTitle("Driver Found")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);



        notificationManager.notify(0, notificationBuilder.build());


    }

    @TargetApi(26)
    private void createChannel(NotificationManager notificationManager) {
        String name = "Uber Goods";
        String description = "Notification";
        int importance = NotificationManager.IMPORTANCE_DEFAULT;

        NotificationChannel mChannel = new NotificationChannel(name, name, importance);
        mChannel.setDescription(description);
        mChannel.enableLights(true);
        mChannel.setLightColor(Color.BLUE);
        notificationManager.createNotificationChannel(mChannel);
    }
}
