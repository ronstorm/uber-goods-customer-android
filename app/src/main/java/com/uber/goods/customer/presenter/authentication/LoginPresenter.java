package com.uber.goods.customer.presenter.authentication;

import android.content.Context;
import android.widget.Toast;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.BuildConfig;
import com.uber.goods.customer.model.authentication.CustomerLoginResponse;
import com.uber.goods.customer.model.authentication.LoginCredentials;
import com.uber.goods.customer.model.authentication.LoginResponse;
import com.uber.goods.customer.model.user.Customer;
import com.uber.goods.customer.network.service.LoginService;
import com.uber.goods.customer.view.authentication.LoginIterface;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

/**
 * Created by anonymous on 12/5/17.
 * Login Activity Presenter
 */

public class LoginPresenter {
    private final Context context;
    private final LoginIterface loginIterface;

    public LoginPresenter(Context context, LoginIterface loginIterface) {
        this.context = context;
        this.loginIterface = loginIterface;
    }

    public void customerLogin(String username, String password) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.create(LoginService.class)
                .login(username, password, "password"), new Observer<CustomerLoginResponse>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(CustomerLoginResponse customerLoginResponse) {
                AppEngine.getInstance().sharedPrefUtils.putPref(AppEngine.getInstance().constants.OAUTH_ACCESS_TOKEN, customerLoginResponse.getAccessToken(), context);
                AppEngine.getInstance().sharedPrefUtils.putPref(AppEngine.getInstance().constants.OAUTH_TOKEN_TYPE, customerLoginResponse.getTokenType(), context);
                AppEngine.getInstance().sharedPrefUtils.putPref(AppEngine.getInstance().constants.OAUTH_REFRESH_TOKEN, customerLoginResponse.getRefreshToken(), context);
                loginIterface.customerLogin(customerLoginResponse);
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void loadCustomerProfile() {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(LoginService.class, context).userProfile(),
                new Observer<Customer>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Customer customer) {
                        loginIterface.userProfile(customer);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        loginIterface.error();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public int validation(String username, String password) {
        if(username.isEmpty() || password.isEmpty()) return AppEngine.getInstance().constants.LOGIN_EMPTY_FIELD;
        return 0;
    }
}
