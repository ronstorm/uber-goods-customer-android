package com.uber.goods.customer.activity.issue;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.model.issue.Topic;

import java.util.List;

/**
 * Created by bitto on 10-Apr-18.
 */

public class ReportIssueSubItemView {

    public void setView(final Activity activity, final Context context, LinearLayout menuList, final List<Topic> topics) {
        LayoutInflater inflater = LayoutInflater.from(context);
        for (int i = 0; i<topics.size(); i++) {
            View view  = null;
            view = inflater.inflate(R.layout.activity_report_problem_sub_menu_item, menuList, false);

            SubMenuItemViewHolder viewHolder = new SubMenuItemViewHolder();

            viewHolder.button = view.findViewById(R.id.reportSubItem);
            viewHolder.navButtonIcon = view.findViewById(R.id.reportSubItemImg);
            view.setTag(viewHolder);

            SubMenuItemViewHolder holder = (SubMenuItemViewHolder) view.getTag();
            holder.button.setText(topics.get(i).getName());

            final int finalI = i;
            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ReportIssueSubmitActivity.class);
                    intent.putExtra("topic", topics.get(finalI).getId());
                    context.startActivity(intent);
                }
            });
            menuList.addView(view);
            menuList.setVisibility(View.GONE);
        }
    }

}

class SubMenuItemViewHolder {
    public Button button;
    public ImageView navButtonIcon;
}
