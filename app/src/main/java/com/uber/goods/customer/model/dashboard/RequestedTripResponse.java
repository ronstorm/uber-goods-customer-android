package com.uber.goods.customer.model.dashboard;

/**
 * Created by kud-wtag on 6/12/18.
 */

public class RequestedTripResponse {
    private  int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
