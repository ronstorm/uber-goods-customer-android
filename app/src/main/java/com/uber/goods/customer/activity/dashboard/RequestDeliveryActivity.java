package com.uber.goods.customer.activity.dashboard;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.MyOptionsPickerView;
import com.bigkoo.pickerview.listener.OnDismissListener;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.activity.BaseUIController;
import com.uber.goods.customer.model.dashboard.GoodsCategory;
import com.uber.goods.customer.model.dashboard.GoodsSubCategory;
import com.uber.goods.customer.model.dashboard.TripEstimate;
import com.uber.goods.customer.model.dashboard.TripEstimateResponse;
import com.uber.goods.customer.model.dashboard.TripRequest;
import com.uber.goods.customer.presenter.dashboard.RequestRiderPresenter;
import com.uber.goods.customer.util.TImerUtilInterface;
import com.uber.goods.customer.view.dashboard.RequestRiderInterface;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by bitto on 14-May-18.
 */

public class RequestDeliveryActivity extends BaseUIController implements RequestRiderInterface {

    @InjectView(R.id.requestDeliveryBtn)
    public Button requestDeliveryBtn;

    @InjectView(R.id.goodType)
    public EditText goodType;

    @InjectView(R.id.subCategory)
    public EditText subCategory;

    @InjectView(R.id.pickUp)
    public EditText pickUp;

    @InjectView(R.id.destination)
    public EditText destination;

    @InjectView(R.id.productSize)
    public EditText productSize;

    @InjectView(R.id.productWeight)
    public EditText productWeight;

    @InjectView(R.id.quantity)
    public EditText quantity;

    @InjectView(R.id.deliveryType)
    public EditText deliveryType;

    @InjectView(R.id.recurrenceType)
    public EditText recurrenceType;

    @InjectView(R.id.needLoadUnload)
    public EditText needLoadUnload;

    @InjectView(R.id.estimateCost)
    public LinearLayout estimateCost;

    @InjectView(R.id.tripEstimationText)
    public TextView tripEstimationText;

    @InjectView(R.id.TOC)
    RadioButton toc;

    private boolean tocStatus = false;

    private Context context;

    private RequestRiderPresenter requestRiderPresenter;
    private MyOptionsPickerView singlePicker;
    private ArrayList<String> goodCategoriesList;
    private List<GoodsCategory> goodCategoriesObjectList;
    private int pickerViewId = 0;
    private Integer goodTypeId = null;
    private Integer goodsSubCat = null;
    private Integer deliveryTypeId = null;
    private Integer recurrenceTypeId = null;
    private Integer needLoadUnloadId = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.TOPBAR_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_request_delivery);
        this.init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        ButterKnife.inject(this);
        this.setNavBarTitle("");
        this.context = this;
        requestRiderPresenter = new RequestRiderPresenter(this, this);
        goodCategoriesList = new ArrayList<>();
        singlePicker = new MyOptionsPickerView(context);
        requestRiderPresenter.loadGoodsCategories();
    }

    @OnClick(R.id.requestDeliveryBtn)
    public void requestDeliveryBtnClick() {
        showloading();
        TripRequest tripRequest = new TripRequest();
        tripRequest.setPickupAddress(pickUp.getText().toString());
        tripRequest.setDestinationAddress(destination.getText().toString());
        tripRequest.setGoodCategory(goodTypeId!=null? goodCategoriesObjectList.get(goodTypeId).getId() : null);
        tripRequest.setGoodSubCategory(goodsSubCat!=null? goodCategoriesObjectList.get(goodTypeId).getSubCategories().get(goodsSubCat).getId() : null);
        tripRequest.setProductSize(extractNumber(productSize.getText().toString()));
        tripRequest.setProductWeight(extractNumber(productWeight.getText().toString()));
        tripRequest.setWorkerQuantity(extractNumber(quantity.getText().toString()));
        tripRequest.setDeliveryType(String.valueOf(deliveryTypeId));
        tripRequest.setRecurrenceType(String.valueOf(recurrenceTypeId));
        //tripRequest.setDepartureTime(chooseDateTime.getText().toString());

        if(recurrenceTypeId == 0) {
            requestRiderPresenter.submitTripRequest(tripRequest);
        } else {
            requestRiderPresenter.submitRecurringTripRequest(tripRequest);
        }
    }

    @OnClick(R.id.goodType)
    public void goodTypeOnClick() {
        pickerViewId = 0;
        singlePicker = new MyOptionsPickerView(context);
        ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextSize(15);
        ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextSize(15);
        ((TextView) singlePicker.findViewById(R.id.tvTitle)).setTextSize(15);
        singlePicker.setPicker(goodCategoriesList);
        singlePicker.setTitle("Goods Category");
        singlePicker.setCyclic(false);
        singlePicker.setSelectOptions(filterIndexGoodsCategory(goodType.getText().toString()));
        singlePicker.setOnoptionsSelectListener(new MyOptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                if(!goodType.getText().toString().equals(goodCategoriesList.get(options1))) {
                    goodType.setText(goodCategoriesList.get(options1));
                    subCategory.setText("");
                    goodsSubCat = null;
                    goodTypeId = options1;
                }
            }
        });
        singlePicker.show();
    }

    @OnClick(R.id.subCategory)
    public void subCategoryOnClick() {
        pickerViewId = 1;
        if(goodCategoriesList.size()>0) {
            singlePicker = new MyOptionsPickerView(context);
            ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextColor(getResources().getColor(R.color.colorAccent));
            ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextColor(getResources().getColor(R.color.colorAccent));
            ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextSize(15);
            ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextSize(15);
            ((TextView) singlePicker.findViewById(R.id.tvTitle)).setTextSize(15);
            singlePicker.setPicker(extractGoodsSubCategoryName(goodCategoriesObjectList.get(filterIndexGoodsCategory(goodType.getText().toString())).getSubCategories()));
            singlePicker.setTitle("Goods Sub Category");
            singlePicker.setCyclic(false);
            singlePicker.setSelectOptions((goodCategoriesList.size()>0? filterIndexGoodsCategory(goodType.getText().toString()) : 0));
            singlePicker.setOnoptionsSelectListener(new MyOptionsPickerView.OnOptionsSelectListener() {
                @Override
                public void onOptionsSelect(int options1, int option2, int options3) {
                    if(goodCategoriesObjectList.get(goodTypeId).getSubCategories().size()>0) {
                        subCategory.setText(goodCategoriesObjectList.get(goodTypeId).getSubCategories().get(options1).getName());
                        goodsSubCat = options1;
                    }
                }
            });
            singlePicker.show();
        }
    }

    @OnClick(R.id.deliveryType)
    public void deliveryTypeOnClick() {
        pickerViewId = 2;
        singlePicker = new MyOptionsPickerView(context);
        ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextSize(15);
        ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextSize(15);
        ((TextView) singlePicker.findViewById(R.id.tvTitle)).setTextSize(15);
        final ArrayList<String> items = new ArrayList<String>();
        items.add("Normal");
        items.add("Urgent");
        singlePicker.setPicker(items);
        singlePicker.setTitle("Delivery Type");
        singlePicker.setCyclic(false);
        singlePicker.setSelectOptions(deliveryTypeId==null? 0 : deliveryTypeId);
        singlePicker.setOnoptionsSelectListener(new MyOptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                deliveryType.setText(items.get(options1));
                deliveryTypeId = options1;
            }
        });
        singlePicker.show();
    }

    @OnClick(R.id.recurrenceType)
    public void recurrenceTypeOnClick() {
        pickerViewId = 3;
        singlePicker = new MyOptionsPickerView(context);
        ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextSize(15);
        ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextSize(15);
        ((TextView) singlePicker.findViewById(R.id.tvTitle)).setTextSize(15);
        final ArrayList<String> items = new ArrayList<String>();
        items.add("Once");
        items.add("Daily");
        items.add("Weekly");
        items.add("Monthly");
        singlePicker.setPicker(items);
        singlePicker.setTitle("Recurrence Type");
        singlePicker.setCyclic(false);
        singlePicker.setSelectOptions(recurrenceTypeId==null? 0 : recurrenceTypeId);
        singlePicker.setOnoptionsSelectListener(new MyOptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                recurrenceType.setText(items.get(options1));
                recurrenceTypeId = options1;
            }
        });
        singlePicker.show();
    }

    @OnClick(R.id.needLoadUnload)
    public void needLoadUnloadOnClick() {
        pickerViewId = 4;
        singlePicker = new MyOptionsPickerView(context);
        ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextColor(getResources().getColor(R.color.colorAccent));
        ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextSize(15);
        ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextSize(15);
        ((TextView) singlePicker.findViewById(R.id.tvTitle)).setTextSize(15);
        final ArrayList<String> items = new ArrayList<String>();
        items.add("Yes");
        items.add("No");
        singlePicker.setPicker(items);
        singlePicker.setTitle("Need Load/Unload");
        singlePicker.setCyclic(false);
        singlePicker.setSelectOptions(needLoadUnloadId==null? 0 : needLoadUnloadId);
        singlePicker.setOnoptionsSelectListener(new MyOptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                needLoadUnload.setText(items.get(options1));
                needLoadUnloadId = options1;
            }
        });
        singlePicker.show();
    }

    @OnClick(R.id.pickUp)
    public void pickUpOnClick() {
        Intent intent = null;
        try {
            intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(this);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
        startActivityForResult(intent, 69);
    }

    @OnClick(R.id.destination)
    public void destinationOnClick() {
        Intent intent = null;
        try {
            intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(this);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
        startActivityForResult(intent, 96);
    }

    @OnClick(R.id.estimateCost)
    public void estimateCostOnClick() {

        showloading();
        TripEstimate tripEstimate = new TripEstimate();
        tripEstimate.setPickupAddress(pickUp.getText().toString());
        tripEstimate.setDestinationAddress(destination.getText().toString());
        tripEstimate.setGoodCategory(goodTypeId!=null? goodCategoriesObjectList.get(goodTypeId).getId() : null);
        tripEstimate.setGoodSubCategory(goodsSubCat!=null? goodCategoriesObjectList.get(goodTypeId).getSubCategories().get(goodsSubCat).getId() : null);
        tripEstimate.setProductSize(extractNumber(productSize.getText().toString()));
        tripEstimate.setProductWeight(extractNumber(productWeight.getText().toString()));
        tripEstimate.setWorkerQuantity(extractNumber(quantity.getText().toString()));
        tripEstimate.setDeliveryType(String.valueOf(deliveryTypeId));

        requestRiderPresenter.estimateTrip(tripEstimate);
    }

    @OnClick(R.id.TOC)
    public void tocClick() {
        this.toc.setChecked(tocStatus = !tocStatus);
    }

    @Override
    public void setGoodsCategory(List<GoodsCategory> goodsCategories) {
        goodCategoriesObjectList = goodsCategories;
        goodCategoriesList = extractGoodsCategoryName(goodsCategories);
        if(singlePicker.isShowing() && pickerViewId==0) {
            singlePicker.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(Object o) {
                    singlePicker = new MyOptionsPickerView(context);
                    ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextColor(getResources().getColor(R.color.colorAccent));
                    ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextColor(getResources().getColor(R.color.colorAccent));
                    ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextSize(15);
                    ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextSize(15);
                    ((TextView) singlePicker.findViewById(R.id.tvTitle)).setTextSize(15);
                    singlePicker.setPicker(goodCategoriesList);
                    singlePicker.setTitle("Good Category");
                    singlePicker.setCyclic(false);
                    singlePicker.setSelectOptions(0);
                    singlePicker.setOnoptionsSelectListener(new MyOptionsPickerView.OnOptionsSelectListener() {
                        @Override
                        public void onOptionsSelect(int options1, int option2, int options3) {
                            if(!goodType.getText().toString().equals(goodCategoriesList.get(options1))) {
                                goodType.setText(goodCategoriesList.get(options1));
                                subCategory.setText("");
                                goodsSubCat = null;
                                goodTypeId = options1;
                            }
                        }
                    });
                    singlePicker.show();
                }
            }).dismiss();
        }
    }

    @Override
    public void tripRequestSubmitted(TripRequest tripRequest) {
        hideloading();
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result","data");
        AppEngine.getInstance().sharedPrefUtils.putPref("tripId", String.valueOf(tripRequest.getId()), context);
        Toast.makeText(context, "Trip Id #"+tripRequest.getId(), Toast.LENGTH_SHORT).show();
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    @Override
    public void tripRequestSubmitError() {
        hideloading();
    }

    @Override
    public void tripEstimationError() {
        hideloading();
    }

    @Override
    public void tripEstimationSuccess(TripEstimateResponse tripEstimateResponse) {
        hideloading();
        tripEstimationText.setText("Estimate Cost "+tripEstimateResponse.getFare());
    }

    private ArrayList<String> extractGoodsCategoryName(List<GoodsCategory> goodsCategories) {
        ArrayList<String> goodsCategoriesName = new ArrayList<>();
        for(int i=0; i<goodsCategories.size(); i++) {
            goodsCategoriesName.add(goodsCategories.get(i).getName());
        }
        return goodsCategoriesName;
    }

    private ArrayList<String> extractGoodsSubCategoryName(List<GoodsSubCategory> goodsCategories) {
        ArrayList<String> goodsCategoriesName = new ArrayList<>();
        for(int i=0; i<goodsCategories.size(); i++) {
            goodsCategoriesName.add(goodsCategories.get(i).getName());
        }
        return goodsCategoriesName;
    }

    private int filterIndexGoodsCategory(String goodCategory) {
        int goodsCategoryIndex = 0;
        for(int i=0; i<goodCategoriesList.size(); i++) {
            if(goodCategoriesList.get(i).equals(goodCategory)) {
                goodsCategoryIndex = i;
                break;
            }
        }
        return goodsCategoryIndex;
    }

    private Integer extractNumber(String number) {
        if(number.equals("")) {
            return null;
        } else {
            return Integer.parseInt(number);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 69) {
            if(resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                pickUp.setText(place.getAddress());
            }
        } else if(requestCode == 96) {
            if(resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                destination.setText(place.getAddress());
            }
        }
    }
}
