package com.uber.goods.customer.activity.mytrips;

/**
 * Created by kud-wtag on 6/26/18.
 */

public class Rating {
    private int trip;
    private int rating;
    private String comment;

    public int getTrip() {
        return trip;
    }
    public void setTrip(int trip) {
        this.trip = trip;
    }
    public int getRating() {
        return rating;
    }
    public void setRating(int rating) {
        this.rating = rating;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
}
