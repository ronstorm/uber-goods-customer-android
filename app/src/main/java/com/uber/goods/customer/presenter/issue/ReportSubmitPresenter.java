package com.uber.goods.customer.presenter.issue;

import android.content.Context;
import android.widget.Toast;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.model.issue.IssueCreateRequest;
import com.uber.goods.customer.model.issue.IssueCreateResponse;
import com.uber.goods.customer.network.service.IssueApiService;
import com.uber.goods.customer.view.issue.ReportIssueSubmitInterface;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by bitto on 13-May-18.
 */

public class ReportSubmitPresenter {
    private Context context;
    private ReportIssueSubmitInterface reportIssueSubmitInterface;

    public ReportSubmitPresenter(Context context, ReportIssueSubmitInterface reportIssueSubmitInterface) {
        this.context = context;
        this.reportIssueSubmitInterface = reportIssueSubmitInterface;
    }

    public void submitIssue(IssueCreateRequest issueCreateRequest) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(IssueApiService.class, context).submitIssue(issueCreateRequest),
                new Observer<IssueCreateResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(IssueCreateResponse issueCreateResponse) {
                        Toast.makeText(context, "Issue Submitted Successfully", Toast.LENGTH_LONG).show();
                        reportIssueSubmitInterface.onSubmitApiSuccess(issueCreateResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        reportIssueSubmitInterface.onSubmitApiError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
