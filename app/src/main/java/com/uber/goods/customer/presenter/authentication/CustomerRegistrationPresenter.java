package com.uber.goods.customer.presenter.authentication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bigkoo.pickerview.MyOptionsPickerView;
import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.BuildConfig;
import com.uber.goods.customer.R;
import com.uber.goods.customer.model.GeoLocationResponse;
import com.uber.goods.customer.model.user.Customer;
import com.uber.goods.customer.network.service.CustomerRegistrationApiService;
import com.uber.goods.customer.view.authentication.CustomerRegistrationInterface;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by anonymous on 3/7/18.
 */

public class CustomerRegistrationPresenter {

    private Context context;
    private Activity activity;
    private CustomerRegistrationInterface customerRegistrationInterface;
    private String profilePictureFilePath;

    public CustomerRegistrationPresenter(Activity activity,Context context, CustomerRegistrationInterface customerRegistrationInterface) {
        this.context = context;
        this.activity = activity;
        this.customerRegistrationInterface = customerRegistrationInterface;
    }

    public void getDefaultLocation(String latLng) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.create(CustomerRegistrationApiService.class, AppEngine.getInstance().constants.GOOGLE_MAP_GEO_CODING_BASE_URL)
                .getAddressFromLatLng(latLng, BuildConfig.GoogleApiKey), new Observer<GeoLocationResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }
            @Override
            public void onError(@NonNull Throwable e) {
                System.out.println(e.toString());
            }
            @Override
            public void onComplete() {

            }
            @Override
            public void onNext(@NonNull GeoLocationResponse geoLocationResponse) {
                System.out.println(geoLocationResponse.getResults().size());
                if(geoLocationResponse.getResults().size()>0) {
                    customerRegistrationInterface.setDefaultLocation(geoLocationResponse.getResults().get(0).getFormatted_address());
                }
            }
        });
    }

    public void requestPermission(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppEngine.getInstance().constants.STORAGE_PERMISSION_CODE);
        } else {
            AppEngine.getInstance().androidIntent.startCameraActivity(activity, AppEngine.getInstance().constants.MESSAGE_ACTIVITY_RESULT_CODE_CAMERA);
        }
    }

    public void uploadOptionSelectRusult(int requestCode, int resultCode, Intent data, ImageView showProfilePicture, RelativeLayout uploadOptionHolder, LinearLayout uploadOption) {
        if(requestCode == AppEngine.getInstance().constants.MESSAGE_ACTIVITY_RESULT_CODE_FILE && resultCode==RESULT_OK) {
            showProfilePicture.setImageBitmap(AppEngine.getInstance().storageImageHelper.getImagefromURI(this.activity, data));
            //profilePictureFilePath = data.getData().getPath();
        }
        else if (requestCode == AppEngine.getInstance().constants.MESSAGE_ACTIVITY_RESULT_CODE_CAMERA && resultCode == RESULT_OK) {
            showProfilePicture.setImageBitmap(AppEngine.getInstance().storageImageHelper.getBitmapFromCamera(this.activity, data));
            //profilePictureFilePath = AppEngine.getInstance().storageImageHelper.getRealPathFromURI(activity, data.getData());
        }
        uploadOptionHolder.setVisibility(View.INVISIBLE);
        AppEngine.getInstance().animation.slideUp(uploadOption, 500);
    }

    public MyOptionsPickerView initPicker(Context context, final EditText gender) {
        MyOptionsPickerView singlePicker = new MyOptionsPickerView(context);
        ((Button) singlePicker.findViewById(R.id.btnCancel)).setTextColor(context.getResources().getColor(R.color.colorAccent));
        ((Button) singlePicker.findViewById(R.id.btnSubmit)).setTextColor(context.getResources().getColor(R.color.colorAccent));
        final ArrayList<String> items = new ArrayList<String>();
        items.add("Male");
        items.add("Female");
        items.add("Other");
        singlePicker.setPicker(items);
        singlePicker.setTitle("Gender");
        singlePicker.setCyclic(false);
        singlePicker.setSelectOptions(0);
        singlePicker.setOnoptionsSelectListener(new MyOptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                gender.setText(items.get(options1));
            }
        });
        return  singlePicker;
    }

    public void addProfilePictureClick(Context context, LinearLayout uploadOption, RelativeLayout uploadOptionHolder) {
        if(!AppEngine.getInstance().animation.isFading()) {
            AppEngine.getInstance().animation.slideUp(uploadOption, 500);
            AppEngine.getInstance().animation.setInitFadeing(true);
            AppEngine.getInstance().animation.fadeIn(context, uploadOptionHolder);
        }
    }

    public void uploadOptionHolderClick(Context context, LinearLayout uploadOption, RelativeLayout uploadOptionHolder) {
        if(!AppEngine.getInstance().animation.isFading()) {
            AppEngine.getInstance().animation.slideDown(uploadOption, 500);
            AppEngine.getInstance().animation.setInitFadeing(true);
            AppEngine.getInstance().animation.fadeAway(context, uploadOptionHolder);
        }
    }

    public void registerUser(final Customer customer) {
//        File file = new File(profilePictureFilePath);
//        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
//        MultipartBody.Part mulPart = MultipartBody.Part.createFormData("user.picture", file.getName(), requestBody);
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.create(CustomerRegistrationApiService.class).register(
//                customer.getUser().getName(),
//                customer.getUser().getEmail(),
//                customer.getUser().getPassword(),
//                mulPart,
//                customer.getGender(),
//                customer.getBirthDate(),
//                String.valueOf(customer.getNationality()),
//                customer.getAddress(),
//                customer.getMobileNumber()
                customer
                ),
                new Observer<Response<Customer>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<Customer> customerResponse) {
                        if(customerResponse.isSuccessful()) customerRegistrationInterface.registrationSuccess(customerResponse.body());
                        else {
                            Toast.makeText(context, customerResponse.message(), Toast.LENGTH_LONG).show();
                            //Toast.makeText(context, customerResponse.headers()., Toast.LENGTH_LONG).show();
                            try {
                                Toast.makeText(context, customerResponse.errorBody().string(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            customerRegistrationInterface.restrationError();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
