package com.uber.goods.customer.model.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripEstimateResponse {
    @SerializedName("fare")
    @Expose
    private float fare;

    public float getFare() {
        return fare;
    }

    public void setFare(float fare) {
        this.fare = fare;
    }
}
