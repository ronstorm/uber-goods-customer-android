package com.uber.goods.customer.activity.settings;

import android.os.Bundle;
import android.widget.ListView;

import com.uber.goods.customer.AppEngine;
import com.uber.goods.customer.R;
import com.uber.goods.customer.activity.BaseUIController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kud-wtag on 4/15/18.
 */

public class SettingsActivity extends BaseUIController {

    ListView settingsLanguageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.TOPBAR_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_settings_screen);
        this.init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        this.setNavBarTitle("Settings");
        settingsLanguageList = (ListView) findViewById(R.id.settingsLanguageList);
        List<String> languageList = new ArrayList<>();
        languageList.add("Arabic");
        languageList.add("Englist");
        SettingsLanguageListAdapter settingsLanguageListAdapter = new SettingsLanguageListAdapter(this, languageList);
        settingsLanguageList.setAdapter(settingsLanguageListAdapter);
    }
}
