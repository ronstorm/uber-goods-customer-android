package com.uber.goods.customer.util;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uber.goods.customer.BuildConfig;
import com.uber.goods.customer.R;

/**
 * Created by kud-wtag on 3/8/18.
 */

public class GmapView implements OnMapReadyCallback, GoogleMapLocationListner {

    private static final int LOCATION_UPDATE_MIN_DISTANCE = 0;
    private static final int LOCATION_UPDATE_MIN_TIME = 0;

    private MapView mapView;
    private GoogleMap googleMap;
    private Bundle savedInstanceState;
    private LocationManager mLocationManager;
    private Context context;
    private boolean permission;
    private Activity activity;
    private boolean mapCameraOffset;
    private GoogleMapLocationListner googleMapLocationListner;

    private static final String MAP_VIEW_BUNDLE_KEY = BuildConfig.GoogleApiKey;
    private boolean loadCustomMarker;

    public void initView(Context context, MapView mapView, Bundle savedInstanceState, boolean permission) {
        this.context = context;
        this.savedInstanceState = savedInstanceState;
        Bundle mapViewBundle = null;
        if (this.savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        this.mapView = mapView;
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);
        this.permission = permission;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.mapView.onResume();
        this.googleMap.setMinZoomPreference(12);
        LatLng ny = new LatLng(40.7143528, -74.0059731);
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLng(ny));
        Log.d("Map Ready", "Mapready");
    }

    private void drawMarker(Location location) {
        String currentPosition = context.getResources().getString(R.string.share_location_cur_loc_text);
        if (this.googleMap != null) {
            this.googleMap.clear();
            LatLng gps = new LatLng(location.getLatitude(), location.getLongitude());
            this.googleMap.addMarker(new MarkerOptions()
                    .position(gps)
                    .title(currentPosition));
            this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(gps, 12));
        }
    }

    public GoogleMap getGoogleMap() {
        return this.googleMap;
    }

    private void drawMarkerOffset(Location location) {
        String currentPosition = context.getResources().getString(R.string.share_location_cur_loc_text);
        if (this.googleMap != null) {
            this.googleMap.clear();
            LatLng gps = new LatLng(location.getLatitude(), location.getLongitude());
            this.googleMap.addMarker(new MarkerOptions()
                    .position(gps)
                    .title(currentPosition));

            LatLng target = new LatLng(location.getLatitude(), location.getLongitude());
            Projection projection = googleMap.getProjection();
            Point screenLocation = projection.toScreenLocation(target);
            screenLocation.x += 80;
            screenLocation.y += 120;
            LatLng offsetTarget = projection.fromScreenLocation(screenLocation);
            this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(offsetTarget, 12));
        }
    }

    public void setMapCameraOffset(boolean mapCameraOffset) {
        this.mapCameraOffset = mapCameraOffset;
    }

    public void setGoogleMapLocationListner(GoogleMapLocationListner googleMapLocationListner) {
        this.googleMapLocationListner = googleMapLocationListner;
    }

    public void setLoadCustomMarker(boolean loadCustomMarker) {
        this.loadCustomMarker = loadCustomMarker;
    }

    public MapView getMapView() {
        return mapView;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            if (loadCustomMarker && googleMapLocationListner != null) {
                googleMapLocationListner.onLocationChanged(location);
            } else {
                if (!mapCameraOffset) drawMarker(location);
                else drawMarkerOffset(location);
            }
        }
    }

    /**
     * Method to animate marker to destination location
     * @param destination destination location (must contain bearing attribute, to ensure
     *                    marker rotation will work correctly)
     * @param marker marker to be animated
     */
    public static void animateMarker(final Location destination, final Marker marker) {
        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(1000); // duration 1 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        marker.setRotation(computeRotation(v, startRotation, destination.getBearing()));
                    } catch (Exception ex) {
                        // I don't care atm..
                    }
                }
            });

            valueAnimator.start();
        }
    }

    private static float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }
}

interface LatLngInterpolator {
    LatLng interpolate(float fraction, LatLng a, LatLng b);

    class LinearFixed implements LatLngInterpolator {
        @Override
        public LatLng interpolate(float fraction, LatLng a, LatLng b) {
            double lat = (b.latitude - a.latitude) * fraction + a.latitude;
            double lngDelta = b.longitude - a.longitude;
            // Take the shortest path across the 180th meridian.
            if (Math.abs(lngDelta) > 180) {
                lngDelta -= Math.signum(lngDelta) * 360;
            }
            double lng = lngDelta * fraction + a.longitude;
            return new LatLng(lat, lng);
        }
    }
}