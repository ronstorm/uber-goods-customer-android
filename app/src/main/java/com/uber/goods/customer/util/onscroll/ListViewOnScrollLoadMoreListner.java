package com.uber.goods.customer.util.onscroll;

/**
 * Created by anonymous on 1/11/18.
 */

public interface ListViewOnScrollLoadMoreListner {
    void onScrollLoadMore();
}
