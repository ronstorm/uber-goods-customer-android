package com.uber.goods.customer.view.authentication;

import com.uber.goods.customer.model.authentication.ForgetPasswordResponse;

public interface ForgetPasswordCallback {
    public void success();
    public void error();
}
