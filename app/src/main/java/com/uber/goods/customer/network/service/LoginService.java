package com.uber.goods.customer.network.service;



import com.uber.goods.customer.BuildConfig;
import com.uber.goods.customer.model.authentication.CustomerLoginResponse;
import com.uber.goods.customer.model.authentication.LoginCredentials;
import com.uber.goods.customer.model.authentication.LoginResponse;
import com.uber.goods.customer.model.notification.FirebaseRegistration;
import com.uber.goods.customer.model.user.Customer;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by anonymous on 12/4/17.
 *
 */

public interface LoginService {
    String autorization = BuildConfig.BasicAuth;

    @FormUrlEncoded
    @POST("/o/token/?profile_type=Customer")
    @Headers({
            autorization
    })
    Observable<CustomerLoginResponse> login(@Field("username") String username, @Field("password") String password, @Field("grant_type") String grantType);

    @FormUrlEncoded
    @POST("/o/token/")
    Call<CustomerLoginResponse> refreshToken(@Field("refresh_token") String refreshToken, @Field("client_id") String clientId, @Field("client_secret") String clientSecret, @Field("grant_type") String grantType);

    @GET("/api/v1/customer/profile/")
    Observable<Customer> userProfile();

    @POST("/api/v1/fcm/")
    Observable<FirebaseRegistration> fcmRegistration(@Body FirebaseRegistration firebaseRegistration);
}